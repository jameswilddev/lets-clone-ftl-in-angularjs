describe('reactorService', function(){
    beforeEach(module('reactorService'));
    it('creates the expected fields with the expected values when asked to initialize an empty model', inject(function(reactorService){
        var model = {};
        reactorService.initialize(model);
        expect(model).toEqual({
            reactor: {
                allocated: 0,
                total: 5
            }
        })
    }));
    it('allows allocations below the limit', inject(function(reactorService){
        var model = {
            reactor:{
                allocated: 2,
                total: 5
            }
        };
        expect(reactorService.requestPower(model, 1)).toBeTruthy();
        expect(model).toEqual({
            reactor:{
                allocated: 3,
                total: 5
            }
        });
    }));
    
    it('allows allocations to the limit', inject(function(reactorService){
        var model = {
            reactor:{
                allocated: 2,
                total: 5
            }
        };
        expect(reactorService.requestPower(model, 3)).toBeTruthy();
        expect(model).toEqual({
            reactor:{
                allocated: 5,
                total: 5
            }
        });
    }));  
    
    it('does not allow allocations beyond the limit', inject(function(reactorService){
        var model = {
            reactor:{
                allocated: 2,
                total: 5
            }
        };
        expect(reactorService.requestPower(model, 4)).toBeFalsy();
        expect(model).toEqual({
            reactor:{
                allocated: 2,
                total: 5
            }
        });
    }));    
    
    it('allows deallocations', inject(function(reactorService){
        var model = {
            reactor:{
                allocated: 4,
                total: 5
            }
        };
        reactorService.releasePower(model, 3);
        expect(model).toEqual({
            reactor:{
                allocated: 1,
                total: 5
            }
        });
    }));     
});