describe('entityService', function(){
    beforeEach(module('entityService'));
    describe('findAt', function(){
        it('returns the entity at the given location', inject(function(entityService){
            var entities = [{
                x: 4,
                y: 7
            }, {
                x: 6, 
                y: 8
            }];
            
            expect(entityService.findAt(entities, 4, 7)).toBe(entities[0]);
        }));
        it('returns undefined when entities are on another column', inject(function(entityService){
            var entities = [{
                x: 3,
                y: 7
            }];
            
            expect(entityService.findAt(entities, 4, 7)).toBeUndefined();
        }));  
        it('returns undefined when entities are on another row', inject(function(entityService){
            var entities = [{
                x: 4,
                y: 6
            }];
            
            expect(entityService.findAt(entities, 4, 7)).toBeUndefined();
        }));          
    }); 
    describe('approach', function(){
        var location = {
            x: 4,
            y: 7,
        };
        it('does not move if in position', inject(function(entityService){
            var entityModel = {
                x: 4,
                y: 7
            };
            
            expect(entityService.approach(entityModel, location)).toBeFalsy();
            
            expect(entityModel.x).toEqual(4);
            expect(entityModel.y).toEqual(7);
        }));
        it('moves left if to the right', inject(function(entityService){
            var entityModel = {
                x: 6,
                y: 7
            };
            
            expect(entityService.approach(entityModel, location)).toBeTruthy();
            
            expect(entityModel.x).toEqual(5);
            expect(entityModel.y).toEqual(7);
        }));  
        it('moves right if to the left', inject(function(entityService){
            var entityModel = {
                x: 2,
                y: 7
            };
            
            expect(entityService.approach(entityModel, location)).toBeTruthy();
            
            expect(entityModel.x).toEqual(3);
            expect(entityModel.y).toEqual(7);
        }));    
        it('moves up if below', inject(function(entityService){
            var entityModel = {
                x: 4,
                y: 5
            };
            
            expect(entityService.approach(entityModel, location)).toBeTruthy();
            
            expect(entityModel.x).toEqual(4);
            expect(entityModel.y).toEqual(6);
        }));       
        it('moves down if above', inject(function(entityService){
            var entityModel = {
                x: 4,
                y: 9
            };
            
            expect(entityService.approach(entityModel, location)).toBeTruthy();
            
            expect(entityModel.x).toEqual(4);
            expect(entityModel.y).toEqual(8);
        }));        
        it('only moves one tile at a time even if separated on multiple axes', inject(function(entityService, vectorService){
            var entityModel = {
                x: 6,
                y: 9
            };
            
            entityService.approach(entityModel, location);
            
            var difference = {};
            vectorService.subtract(entityModel, {x: 6, y: 9}, difference);
            expect(vectorService.magnitudeSquared(difference)).toEqual(1);
        }));          
    });
});