describe('shipService', function(){
    beforeEach(module('shipService'));
    describe('import', function(){
        it('populates the room models from the room templates', inject(function(shipService, roomService){
            spyOn(roomService, 'import').and.callFake(function(shipTemplateArg, roomTemplate){
                expect(shipTemplateArg).toBe(shipTemplate);
                if(roomTemplate == 'Room Template A') return roomModelA;
                if(roomTemplate == 'Room Template B') return roomModelB;
                expect(false).toBeTruthy();
            }); 
            
            var shipTemplate = {
                rooms: ['Room Template A', 'Room Template B']
            };
            var roomModelA = {};
            var roomModelB = {};
            
            var shipModel = shipService.import(shipTemplate);
            
            expect(roomService.import).toHaveBeenCalledWith(shipTemplate, 'Room Template A');
            expect(roomService.import).toHaveBeenCalledWith(shipTemplate, 'Room Template B');
            
            expect(shipModel.rooms.length).toEqual(2);
            expect(shipModel.rooms[0]).toBe(roomModelA);
            expect(shipModel.rooms[1]).toBe(roomModelB);
        }));
        it('initializes the reactor', inject(function(shipService, reactorService){
            spyOn(reactorService, 'initialize');
    
            var shipTemplate = {};
            
            var shipModel = shipService.import(shipTemplate);
            
            expect(reactorService.initialize).toHaveBeenCalledWith(shipModel);
        }));    
        it('populates the door models from the door templates', inject(function(shipService, doorService){
            spyOn(doorService, 'import').and.callFake(function(shipTemplate, doorTemplate){
                if(doorTemplate == 'Door Template A') return doorModelA;
                if(doorTemplate == 'Door Template B') return doorModelB;
                expect(false).toBeTruthy();
            }); 
            
            var shipTemplate = {
                doors: ['Door Template A', 'Door Template B']
            };
            var doorModelA = {};
            var doorModelB = {};
            
            var shipModel = shipService.import(shipTemplate);
            
            expect(doorService.import).toHaveBeenCalledWith(shipTemplate, 'Door Template A');
            expect(doorService.import).toHaveBeenCalledWith(shipTemplate, 'Door Template B');
            
            expect(shipModel.doors.length).toEqual(2);
            expect(shipModel.doors[0]).toBe(doorModelA);
            expect(shipModel.doors[1]).toBe(doorModelB);
        }));
        it('creates an empty list of crew', inject(function(shipService){
            var shipTemplate = {};
            
            var shipModel = shipService.import(shipTemplate);
            
            expect(shipModel.crew).toEqual([]);
        })); 
        it('creates an empty list of intruders', inject(function(shipService){
            var shipTemplate = {};
            
            var shipModel = shipService.import(shipTemplate);
            
            expect(shipModel.intruders).toEqual([]);
        }));         
        it('creates an empty list of fires', inject(function(shipService){
            var shipTemplate = {};
            
            var shipModel = shipService.import(shipTemplate);
            
            expect(shipModel.fires).toEqual([]);
        })); 
    });
    describe('tick', function(){
        var shipModel = {
            crew: ['Crew A', 'Crew B']
        };
        it('calls crewService.tick for each crewmember', inject(function(shipService, crewService){
            spyOn(crewService, 'tick');
            
            shipService.tick(shipModel);
            
            expect(crewService.tick).toHaveBeenCalledWith(shipModel, 'Crew A');
            expect(crewService.tick).toHaveBeenCalledWith(shipModel, 'Crew B');
        }));
    });
    describe('findEmptyTile', function(){
        var shipModel, output;
        beforeEach(function(){
            shipModel = {rooms: ['Room A', 'Room B', 'Room C']};
            output = {isOutput: true};
        });
        describe('not allowing dangerous rooms', function(){
            it('returns false and does not modify output when all rooms are dangerous', inject(function(shipService, roomService){
                spyOn(roomService, 'dangerous').and.returnValue(true);
                spyOn(roomService, 'findEmptyTile').and.returnValue(false);
                
                var result = shipService.findEmptyTile(shipModel, false, false, output);
                
                expect(result).toBeFalsy();
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', false, output);                      
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);                                      
                expect(output).toEqual({isOutput: true});
            }));
            it('calls roomService.findEmptyTile for each non-dangerous room, but returns false and does not modify output if no rooms contain empty tiles', inject(function(shipService, roomService){
                spyOn(roomService, 'dangerous').and.callFake(function(shipModelArg, roomModel){
                    expect(shipModelArg).toBe(shipModel);
                    return roomModel == 'Room A'
                });
                spyOn(roomService, 'findEmptyTile').and.returnValue(false);
                
                var result = shipService.findEmptyTile(shipModel, false, false, output);
                
                expect(result).toBeFalsy();
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', false, output);
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);                                      
                expect(output).toEqual({isOutput: true});
            }));        
            it('calls roomService.findEmptyTile for each non-dangerous room, but stops and returns true upon finding a room containing empty tiles, also setting the roomId in output', inject(function(shipService, roomService){
                spyOn(roomService, 'dangerous').and.callFake(function(shipModelArg, roomModel){
                    expect(shipModelArg).toBe(shipModel);
                    return roomModel == 'Room A';
                });
                spyOn(roomService, 'findEmptyTile').and.callFake(function(shipModelArg, roomModel, allowDangerousTiles, output){
                    expect(shipModelArg).toBe(shipModel);
                    return roomModel == 'Room B';
                });
                
                var result = shipService.findEmptyTile(shipModel, false, false, output);
                
                expect(result).toBeTruthy();
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', false, output);
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output); 
                expect(output).toEqual({isOutput: true, roomId: 1});
            }));                  
        });
        describe('allowing dangerous rooms', function(){
            describe('and allowing dangerous tiles', function(){
                it('returns false and does not modify output when all rooms are dangerous and none contain empty tiles', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.returnValue(true);
                    spyOn(roomService, 'findEmptyTile').and.returnValue(false);
                    
                    var result = shipService.findEmptyTile(shipModel, true, true, output);
                    
                    expect(result).toBeFalsy();
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', true, output);                     
                    expect(output).toEqual({isOutput: true});
                }));  
                it('returns false and does not modify output when all rooms are dangerous and none contain empty tiles', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.returnValue(true);
                    spyOn(roomService, 'findEmptyTile').and.returnValue(false);
                    
                    var result = shipService.findEmptyTile(shipModel, true, true, output);
                    
                    expect(result).toBeFalsy();
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', true, output);                     
                    expect(output).toEqual({isOutput: true});
                }));  
                it('returns false and does not modify output when some rooms are dangerous and none contain empty tiles', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.callFake(function(roomModel){
                        return roomModel == 'Room A';
                    });
                    spyOn(roomService, 'findEmptyTile').and.returnValue(false);
                    
                    var result = shipService.findEmptyTile(shipModel, true, true, output);
                    
                    expect(result).toBeFalsy();
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', true, output);                                         
                    expect(output).toEqual({isOutput: true});
                }));   
                it('returns false and does not modify output when no rooms are dangerous but none contain empty tiles', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.returnValue(false);
                    spyOn(roomService, 'findEmptyTile').and.returnValue(false);
                    
                    var result = shipService.findEmptyTile(shipModel, true, true, output);
                    
                    expect(result).toBeFalsy();
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', true, output);                                         
                    expect(output).toEqual({isOutput: true});
                }));                 
                it('first attempts to find rooms which are not dangerous, returning true and setting output.roomId if one contains empty tiles', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.callFake(function(shipModelArg, roomModel){
                        expect(shipModelArg).toBe(shipModel);
                        return roomModel == 'Room A';
                    });
                    spyOn(roomService, 'findEmptyTile').and.callFake(function(shipModelArg, roomModel, allowDangerousTiles, output){
                        expect(shipModelArg).toBe(shipModel);
                        return roomModel == 'Room B';
                    });
                    
                    var result = shipService.findEmptyTile(shipModel, true, true, output);
                    
                    expect(result).toBeTruthy();
                        
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', false, output);             
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);     
                    expect(output).toEqual({isOutput: true, roomId: 1});
                }));      
                it('falls back to rooms which are dangerous if all are dangerous or do not contain empty tiles, returning true and setting output.roomId if one contains an empty tile', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.callFake(function(shipModelArg, roomModel){
                        expect(shipModelArg).toBe(shipModel);
                        return roomModel == 'Room A';
                    })
                    spyOn(roomService, 'findEmptyTile').and.callFake(function(shipModelArg, roomModel, allowDangerousTiles, output){
                        expect(shipModelArg).toBe(shipModel);
                        return roomModel != 'Room A';
                    });
                    
                    var result = shipService.findEmptyTile(shipModel, true, true, output);
                    
                    expect(result).toBeTruthy();
                        
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', false, output);    
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);                     
                    expect(output).toEqual({isOutput: true, roomId: 1});
                }));                   
                it('falls back to rooms which are dangerous if all are dangerous, returning true and setting output.roomId if one contains an empty tile', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.returnValue(true);
                    spyOn(roomService, 'findEmptyTile').and.callFake(function(shipModelArg, roomModel, allowDangerousTiles, output){
                        expect(shipModelArg).toBe(shipModel);
                        return roomModel == 'Room B';
                    });
                    
                    var result = shipService.findEmptyTile(shipModel, true, true, output);
                    
                    expect(result).toBeTruthy();
                        
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', false, output);    
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);                     
                    expect(output).toEqual({isOutput: true, roomId: 1});
                }));   
                it('falls back to dangerous tiles in dangerous rooms if all are dangerous, returning true and setting output.roomId if one contains empty tiles', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.returnValue(true);
                    spyOn(roomService, 'findEmptyTile').and.callFake(function(shipModelArg, roomModel, allowDangerousTiles, output){
                        expect(shipModelArg).toBe(shipModel);
                        return roomModel == 'Room B' && allowDangerousTiles == true;
                    });
                    
                    var result = shipService.findEmptyTile(shipModel, true, true, output);
                    
                    expect(result).toBeTruthy();
                        
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', false, output);    
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);                     
                    expect(output).toEqual({isOutput: true, roomId: 1});
                }));          
                it('falls back to dangerous tiles in dangerous rooms if all are dangerous, returning false and not modifying output if none contain empty tiles', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.returnValue(true);
                    spyOn(roomService, 'findEmptyTile').and.returnValue(false);
                    
                    var result = shipService.findEmptyTile(shipModel, true, true, output);
                    
                    expect(result).toBeFalsy();
                        
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', false, output);    
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', true, output);                     
                    expect(output).toEqual({isOutput: true});
                }));                   
            });
            describe('but not allowing dangerous tiles', function(){
                it('returns false when all rooms are dangerous and none contain empty tiles', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.returnValue(true);
                    spyOn(roomService, 'findEmptyTile').and.returnValue(false);
                    
                    var result = shipService.findEmptyTile(shipModel, true, false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', false, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);                                         
                    expect(result).toBeFalsy();
                }));       
                it('returns false when some rooms are dangerous and none contain empty tiles', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.callFake(function(roomModel){
                        return roomModel == 'Room A';
                    });
                    spyOn(roomService, 'findEmptyTile').and.returnValue(false);
                    
                    var result = shipService.findEmptyTile(shipModel, true, false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', false, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);                                         
                    expect(result).toBeFalsy();
                }));   
                it('returns false when no rooms are dangerous but none contain empty tiles', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.returnValue(false);
                    spyOn(roomService, 'findEmptyTile').and.returnValue(false);
                    
                    var result = shipService.findEmptyTile(shipModel, true, false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', false, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);                                         
                    expect(result).toBeFalsy();
                }));                  
                it('first attempts to find rooms which are not dangerous, returning true if one contains empty tiles', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.callFake(function(shipModelArg, roomModel){
                        expect(shipModelArg).toBe(shipModel);
                        return roomModel == 'Room A';
                    });
                    spyOn(roomService, 'findEmptyTile').and.callFake(function(shipModelArg, roomModel, allowDangerousTiles, output){
                        expect(shipModelArg).toBe(shipModel);
                        return roomModel == 'Room B';
                    });
                    
                    var result = shipService.findEmptyTile(shipModel, true, false, output);
                    
                    expect(result).toBeTruthy();
                        
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', false, output);             
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);                      
                })); 
                it('falls back to rooms which are dangerous if all are dangerous or do not contain empty tiles, returning true if one contains an empty tile', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.callFake(function(shipModelArg, roomModel){
                        expect(shipModelArg).toBe(shipModel);
                        return roomModel == 'Room A';
                    })
                    spyOn(roomService, 'findEmptyTile').and.callFake(function(shipModelArg, roomModel, allowDangerousTiles, output){
                        expect(shipModelArg).toBe(shipModel);
                        return roomModel != 'Room A';
                    });
                    
                    var result = shipService.findEmptyTile(shipModel, true, false, output);
                    
                    expect(result).toBeTruthy();
                        
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', false, output);    
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);                     
                }));                   
                it('falls back to rooms which are dangerous if all are dangerous, returning true if one contains an empty tile', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.returnValue(true);
                    spyOn(roomService, 'findEmptyTile').and.callFake(function(shipModelArg, roomModel, allowDangerousTiles, output){
                        expect(shipModelArg).toBe(shipModel);
                        return roomModel == 'Room B';
                    });
                    
                    var result = shipService.findEmptyTile(shipModel, true, false, output);
                    
                    expect(result).toBeTruthy();
                        
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', false, output);    
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);                     
                }));   
                it('falls back to rooms which are dangerous if all are dangerous, returning false if none contain non-dangerous, empty tiles.', inject(function(shipService, roomService){
                    spyOn(roomService, 'dangerous').and.returnValue(true);
                    spyOn(roomService, 'findEmptyTile').and.returnValue(false);
                    
                    var result = shipService.findEmptyTile(shipModel, true, false, output);
                    
                    expect(result).toBeFalsy();
                        
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room A', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room B', false, output);
                    expect(roomService.findEmptyTile).toHaveBeenCalledWith(shipModel, 'Room C', false, output);    
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room A', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room B', true, output);
                    expect(roomService.findEmptyTile).not.toHaveBeenCalledWith(shipModel, 'Room C', true, output);                     
                }));                 
            });
        });
    });
});