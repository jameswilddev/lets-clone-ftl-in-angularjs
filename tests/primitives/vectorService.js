describe("vectorService", function() {
    beforeEach(module('vectorService'));
    describe("utilities", function(){
        describe('clone', function(){
            it('copies x and y between two instances', inject(function(vectorService){
                var out = {};
                vectorService.clone({x: 5, y: 19}, out);
                expect(out).toEqual({x: 5, y: 19});
            }));
        });
    });
    describe("mathematics", function(){
        describe("addition", function(){
            it('with separate parameters generates the correct answer', inject(function(vectorService){
                var out = {};
                vectorService.add({x: 3, y: 4}, {x: 10, y: 15}, out);
                expect(out).toEqual({x: 13, y: 19});
            }));
            it('with a and out the same generates the correct answer', inject(function(vectorService){
                var out = {x: 3, y: 4};
                vectorService.add(out, {x: 10, y: 15}, out);
                expect(out).toEqual({x: 13, y: 19});
            }));  
            it('with b and out the same generates the correct answer', inject(function(vectorService){
                var out = {x: 10, y: 15};
                vectorService.add({x: 3, y: 4}, out, out);
                expect(out).toEqual({x: 13, y: 19});
            }));  
            it('does not modify its inputs', inject(function(vectorService){
                var a = {x: 3, y: 4};
                var b = {x: 10, y: 15}
                vectorService.add(a, b, {});
                expect(a).toEqual({x: 3, y: 4});
                expect(b).toEqual({x: 10, y: 15});
            }));            
        });
        describe("subtraction", function(){
            it('with separate parameters generates the correct answer', inject(function(vectorService){
                var out = {};
                vectorService.subtract({x: 3, y: 4}, {x: 10, y: 15}, out);
                expect(out).toEqual({x: -7, y: -11});
            }));
            it('with a and out the same generates the correct answer', inject(function(vectorService){
                var out = {x: 3, y: 4};
                vectorService.subtract(out, {x: 10, y: 15}, out);
                expect(out).toEqual({x: -7, y: -11});
            }));  
            it('with b and out the same generates the correct answer', inject(function(vectorService){
                var out = {x: 10, y: 15};
                vectorService.subtract({x: 3, y: 4}, out, out);
                expect(out).toEqual({x: -7, y: -11});
            })); 
            it('does not modify its inputs', inject(function(vectorService){
                var a = {x: 3, y: 4};
                var b = {x: 10, y: 15}
                vectorService.subtract(a, b, {});
                expect(a).toEqual({x: 3, y: 4});
                expect(b).toEqual({x: 10, y: 15});
            }));                
        });   
        describe("multiplication", function(){
            it('with separate parameters generates the correct answer', inject(function(vectorService){
                var out = {};
                vectorService.multiply({x: 3, y: 4}, {x: 10, y: 15}, out);
                expect(out).toEqual({x: 30, y: 60});
            }));
            it('with a and out the same generates the correct answer', inject(function(vectorService){
                var out = {x: 3, y: 4};
                vectorService.multiply(out, {x: 10, y: 15}, out);
                expect(out).toEqual({x: 30, y: 60});
            }));  
            it('with b and out the same generates the correct answer', inject(function(vectorService){
                var out = {x: 10, y: 15};
                vectorService.multiply({x: 3, y: 4}, out, out);
                expect(out).toEqual({x: 30, y: 60});
            })); 
            it('does not modify its inputs', inject(function(vectorService){
                var a = {x: 3, y: 4};
                var b = {x: 10, y: 15}
                vectorService.multiply(a, b, {});
                expect(a).toEqual({x: 3, y: 4});
                expect(b).toEqual({x: 10, y: 15});
            }));                
        });   
        describe("division", function(){
            it('with separate parameters generates the correct answer', inject(function(vectorService){
                var out = {};
                vectorService.divide({x: 3, y: 15}, {x: 10, y: 5}, out);
                expect(out).toEqual({x: 0.3, y: 3});
            }));
            it('with a and out the same generates the correct answer', inject(function(vectorService){
                var out = {x: 3, y: 15};
                vectorService.divide(out, {x: 10, y: 5}, out);
                expect(out).toEqual({x: 0.3, y: 3});
            }));  
            it('with b and out the same generates the correct answer', inject(function(vectorService){
                var out = {x: 10, y: 5};
                vectorService.divide({x: 3, y: 15}, out, out);
                expect(out).toEqual({x: 0.3, y: 3});
            })); 
            it('does not modify its inputs', inject(function(vectorService){
                var a = {x: 3, y: 4};
                var b = {x: 10, y: 15}
                vectorService.divide(a, b, {});
                expect(a).toEqual({x: 3, y: 4});
                expect(b).toEqual({x: 10, y: 15});
            }));                
        });        
    });
    describe("component sum", function(){
        it('generates the correct answer', inject(function(vectorService){
            expect(vectorService.componentSum({x: 3, y: 4})).toEqual(7);
        }));
        it('does not modify its input', inject(function(vectorService){
            var input = {x: 3, y: 4};
            vectorService.componentSum(input);
            expect(input).toEqual({x: 3, y: 4});
        }));              
    });
    describe("dot product", function(){
        it('generates the correct answer', inject(function(vectorService){
            expect(vectorService.dotProduct({x: 3, y: 4}, {x: 10, y: 15})).toEqual(90);
        }));
        it('does not modify its inputs', inject(function(vectorService){
            var a = {x: 3, y: 4};
            var b = {x: 10, y: 15};
            vectorService.dotProduct(a, b);
            expect(a).toEqual({x: 3, y: 4});
            expect(b).toEqual({x: 10, y: 15});
        }));              
    }); 
    describe("magnitude squared", function(){
        it('generates the correct answer', inject(function(vectorService){
            expect(vectorService.magnitudeSquared({x: 3, y: 4})).toEqual(25);
        }));
        it('does not modify its input', inject(function(vectorService){
            var input = {x: 3, y: 4};
            vectorService.magnitudeSquared(input);
            expect(input).toEqual({x: 3, y: 4});
        }));              
    });    
    describe("magnitude", function(){
        it('generates the correct answer', inject(function(vectorService){
            expect(vectorService.magnitude({x: 3, y: 4})).toEqual(5);
        }));
        it('does not modify its input', inject(function(vectorService) {
            var input = {x: 3, y: 4};
            vectorService.magnitude(input);
            expect(input).toEqual({x: 3, y: 4});
        }));              
    });  
    
});