describe('boundsService', function(){
    beforeEach(module('boundsService'));
    describe('contains', function(){
        var bounds = {
                left: -3,
                right: 4,
                bottom: -1,
                top: 2
        };
        it('returns true when the point is within the bounds', inject(function(boundsService){
            expect(boundsService.contains(bounds, -1, 0)).toBeTruthy();
        }));
        describe('bordering', function(){
            it('returns true when the point is on the bounds\'s left border', inject(function(boundsService){
                expect(boundsService.contains(bounds, -3, 0)).toBeTruthy();
            }));       
            it('returns true when the point is on the bounds\'s right border', inject(function(boundsService){
                expect(boundsService.contains(bounds, 3, 0)).toBeTruthy();
            }));   
            it('returns true when the point is on the bounds\'s bottom border', inject(function(boundsService){
                expect(boundsService.contains(bounds, -1, -1)).toBeTruthy();
            }));       
            it('returns true when the point is on the bounds\'s top border', inject(function(boundsService){
                expect(boundsService.contains(bounds, 1, 1)).toBeTruthy();
            }));                   
        });
        describe('outside', function(){
            it('returns false when the point is over the bounds\'s left border', inject(function(boundsService){
                expect(boundsService.contains(bounds, -4, 0)).toBeFalsy();
            }));       
            it('returns false when the point is over the bounds\'s right border', inject(function(boundsService){
                expect(boundsService.contains(bounds, 4, 0)).toBeFalsy();
            }));   
            it('returns false when the point is over the bounds\'s bottom border', inject(function(boundsService){
                expect(boundsService.contains(bounds, -1, -2)).toBeFalsy();
            }));       
            it('returns false when the point is over the bounds\'s top border', inject(function(boundsService){
                expect(boundsService.contains(bounds, -1, 2)).toBeFalsy();
            }));                 
        });
    });
    describe('iterate', function(){
        it('iterates through every location in a bounds when the callback returns undefined', inject(function(boundsService){
            var boundsModel = {
                left: 3, 
                right: 7, 
                bottom: -4, 
                top: 1
            };
            var callback = jasmine.createSpy('callback').and.returnValue(undefined);
            
            expect(boundsService.iterate(boundsModel, callback)).toBeUndefined();
            
            expect(callback).toHaveBeenCalledWith(3, -4);
            expect(callback).toHaveBeenCalledWith(4, -4);
            expect(callback).toHaveBeenCalledWith(5, -4);
            expect(callback).toHaveBeenCalledWith(6, -4);
            
            expect(callback).toHaveBeenCalledWith(3, -3);
            expect(callback).toHaveBeenCalledWith(4, -3);
            expect(callback).toHaveBeenCalledWith(5, -3);
            expect(callback).toHaveBeenCalledWith(6, -3);
            
            expect(callback).toHaveBeenCalledWith(3, -2);
            expect(callback).toHaveBeenCalledWith(4, -2);
            expect(callback).toHaveBeenCalledWith(5, -2);
            expect(callback).toHaveBeenCalledWith(6, -2);
            
            expect(callback).toHaveBeenCalledWith(3, -1);
            expect(callback).toHaveBeenCalledWith(4, -1);
            expect(callback).toHaveBeenCalledWith(5, -1);
            expect(callback).toHaveBeenCalledWith(6, -1);
            
            expect(callback).toHaveBeenCalledWith(3, 0);
            expect(callback).toHaveBeenCalledWith(4, 0);
            expect(callback).toHaveBeenCalledWith(5, 0);
            expect(callback).toHaveBeenCalledWith(6, 0);
        }));
        it('iterates through every location until a value (numeric) is returned by the callback, which also stops further iteration', inject(function(boundsService){
            var boundsModel = {
                left: 3, 
                right: 7, 
                bottom: -4, 
                top: 1
            };
            var callback = jasmine.createSpy('callback').and.callFake(function(x, y){
                return x == 4 && y == -2 ? 'Output' : undefined;
            });
            
            expect(boundsService.iterate(boundsModel, callback)).toEqual('Output');
            
            expect(callback).toHaveBeenCalledWith(3, -4);
            expect(callback).toHaveBeenCalledWith(4, -4);
            expect(callback).toHaveBeenCalledWith(5, -4);
            expect(callback).toHaveBeenCalledWith(6, -4);
            
            expect(callback).toHaveBeenCalledWith(3, -3);
            expect(callback).toHaveBeenCalledWith(4, -3);
            expect(callback).toHaveBeenCalledWith(5, -3);
            expect(callback).toHaveBeenCalledWith(6, -3);
            
            expect(callback).toHaveBeenCalledWith(3, -2);
            expect(callback).toHaveBeenCalledWith(4, -2);
            expect(callback).not.toHaveBeenCalledWith(5, -2);
            expect(callback).not.toHaveBeenCalledWith(6, -2);
            
            expect(callback).not.toHaveBeenCalledWith(3, -1);
            expect(callback).not.toHaveBeenCalledWith(4, -1);
            expect(callback).not.toHaveBeenCalledWith(5, -1);
            expect(callback).not.toHaveBeenCalledWith(6, -1);
            
            expect(callback).not.toHaveBeenCalledWith(3, 0);
            expect(callback).not.toHaveBeenCalledWith(4, 0);
            expect(callback).not.toHaveBeenCalledWith(5, 0);
            expect(callback).not.toHaveBeenCalledWith(6, 0);
        }));
        it('iterates through every location until a value (null) is returned by the callback, which also stops further iteration', inject(function(boundsService){
            var boundsModel = {
                left: 3, 
                right: 7, 
                bottom: -4, 
                top: 1
            };
            var callback = jasmine.createSpy('callback').and.callFake(function(x, y){
                return x == 4 && y == -2 ? null : undefined;
            });
            
            expect(boundsService.iterate(boundsModel, callback)).toEqual(null);
            
            expect(callback).toHaveBeenCalledWith(3, -4);
            expect(callback).toHaveBeenCalledWith(4, -4);
            expect(callback).toHaveBeenCalledWith(5, -4);
            expect(callback).toHaveBeenCalledWith(6, -4);
            
            expect(callback).toHaveBeenCalledWith(3, -3);
            expect(callback).toHaveBeenCalledWith(4, -3);
            expect(callback).toHaveBeenCalledWith(5, -3);
            expect(callback).toHaveBeenCalledWith(6, -3);
            
            expect(callback).toHaveBeenCalledWith(3, -2);
            expect(callback).toHaveBeenCalledWith(4, -2);
            expect(callback).not.toHaveBeenCalledWith(5, -2);
            expect(callback).not.toHaveBeenCalledWith(6, -2);
            
            expect(callback).not.toHaveBeenCalledWith(3, -1);
            expect(callback).not.toHaveBeenCalledWith(4, -1);
            expect(callback).not.toHaveBeenCalledWith(5, -1);
            expect(callback).not.toHaveBeenCalledWith(6, -1);
            
            expect(callback).not.toHaveBeenCalledWith(3, 0);
            expect(callback).not.toHaveBeenCalledWith(4, 0);
            expect(callback).not.toHaveBeenCalledWith(5, 0);
            expect(callback).not.toHaveBeenCalledWith(6, 0);
        }));
        it('iterates through every location until a value (false) is returned by the callback, which also stops further iteration', inject(function(boundsService){
            var boundsModel = {
                left: 3, 
                right: 7, 
                bottom: -4, 
                top: 1
            };
            var callback = jasmine.createSpy('callback').and.callFake(function(x, y){
                return x == 4 && y == -2 ? false : undefined;
            });
            
            expect(boundsService.iterate(boundsModel, callback)).toEqual(false);
            
            expect(callback).toHaveBeenCalledWith(3, -4);
            expect(callback).toHaveBeenCalledWith(4, -4);
            expect(callback).toHaveBeenCalledWith(5, -4);
            expect(callback).toHaveBeenCalledWith(6, -4);
            
            expect(callback).toHaveBeenCalledWith(3, -3);
            expect(callback).toHaveBeenCalledWith(4, -3);
            expect(callback).toHaveBeenCalledWith(5, -3);
            expect(callback).toHaveBeenCalledWith(6, -3);
            
            expect(callback).toHaveBeenCalledWith(3, -2);
            expect(callback).toHaveBeenCalledWith(4, -2);
            expect(callback).not.toHaveBeenCalledWith(5, -2);
            expect(callback).not.toHaveBeenCalledWith(6, -2);
            
            expect(callback).not.toHaveBeenCalledWith(3, -1);
            expect(callback).not.toHaveBeenCalledWith(4, -1);
            expect(callback).not.toHaveBeenCalledWith(5, -1);
            expect(callback).not.toHaveBeenCalledWith(6, -1);
            
            expect(callback).not.toHaveBeenCalledWith(3, 0);
            expect(callback).not.toHaveBeenCalledWith(4, 0);
            expect(callback).not.toHaveBeenCalledWith(5, 0);
            expect(callback).not.toHaveBeenCalledWith(6, 0);
        }));
    });    
});