describe('crewService', function(){
    beforeEach(module('crewService'));
    beforeEach(module('vectorService'));
    it('defaults to unselected', inject(function(crewService){
        crewService.names = [null];
        expect(crewService.create().selected).toBeFalsy();
    }));
    describe('names', function(){
        it('downloads the list of names', inject(function(crewService, $httpBackend){
            var names = ['Name A', 'Name B', 'Name C'];
            $httpBackend.when('GET', 'data/crew/names.json').respond(names);
                
            $httpBackend.flush();
            
            expect(crewService.names).toEqual(names);
        }));
        it('shows an error dialog if the names list could not be downloaded', inject(function(crewService, $httpBackend, $window){
            $httpBackend.when('GET', 'data/crew/names.json').respond(404, null);
            spyOn($window, 'alert');
            
            $httpBackend.flush();
            
            expect($window.alert).toHaveBeenCalledWith('Failed to download the list of crew names.');
        }));
    });
    describe('create', function(){
        it('chooses a name at random from the list previously downloaded', inject(function(crewService, $httpBackend, random){
            crewService.names = ['Name A', 'Name B', 'Name C'];
            
            spyOn(random, 'integer').and.returnValue(1);
            
            var crew = crewService.create(3, 6, 8);
            
            expect(random.integer).toHaveBeenCalledWith(0, 2);
            expect(crew.name).toEqual('Name B');
        }));
        it('copies x, y and roomId to the appropiate places', inject(function(crewService){
            crewService.names = ['Test Name'];
            var crewModel = crewService.create(3, 6, 8);
            
            expect(crewModel.x).toEqual(3);
            expect(crewModel.y).toEqual(6);
            expect(crewModel.roomId).toEqual(8);
            expect(crewModel.displayLocation.x).toEqual(3);
            expect(crewModel.displayLocation.y).toEqual(6);
            expect(crewModel.displayLocation.roomId).toEqual(8);
            expect(crewModel.targetLocation.x).toEqual(3);
            expect(crewModel.targetLocation.y).toEqual(6);            
        }));
    });
    describe('whenLoaded', function(){
        it('does not execute the callback until all files have been downloaded', inject(function(crewService){
            var callback = jasmine.createSpy('callback');
            
            crewService.whenLoaded(callback);
            
            expect(callback).not.toHaveBeenCalled();
        }));
        it('executes the callback once all files have been downloaded', inject(function(crewService, $httpBackend){
            var callback = jasmine.createSpy('callback');
            $httpBackend.when('GET', 'data/crew/names.json').respond([]);
            
            crewService.whenLoaded(callback);
            $httpBackend.flush();
            
            expect(callback).toHaveBeenCalled();
        }));        
        it('executes multiple callbacks once all files have been downloaded', inject(function(crewService, $httpBackend){
            var callbackA = jasmine.createSpy('callbackA');
            var callbackB = jasmine.createSpy('callbackB');            
            crewService.whenLoaded(callbackA);
            crewService.whenLoaded(callbackB);
            
            $httpBackend.when('GET', 'data/crew/names.json').respond([]);
            $httpBackend.flush();
            
            expect(callbackA).toHaveBeenCalled();
            expect(callbackB).toHaveBeenCalled();
        }));          
        it('has populated names when the callback is raised', inject(function(crewService, $httpBackend){
            var callback = jasmine.createSpy('callback').and.callFake(function(){
                expect(crewService.names).toEqual([]);
            });
            $httpBackend.when('GET', 'data/crew/names.json').respond([]);
            
            crewService.whenLoaded(callback);
            $httpBackend.flush();
            
            expect(callback).toHaveBeenCalled();
        })); 
        it('executes the callback immediately if all files were previously downloaded', inject(function(crewService, $httpBackend){
            var callback = jasmine.createSpy('callback');
            $httpBackend.when('GET', 'data/crew/names.json').respond([]);
            
            $httpBackend.flush();            
            crewService.whenLoaded(callback);
            
            expect(callback).toHaveBeenCalled();
        }));        
        it('does not execute callbacks registered before download completes if the download fails', inject(function(crewService, $httpBackend){
            var callback = jasmine.createSpy('callback');
            $httpBackend.when('GET', 'data/crew/names.json').respond(404, null);
            
            crewService.whenLoaded(callback);
            $httpBackend.flush();
            
            expect(callback).not.toHaveBeenCalled();
        }));        
        it('does not execute callbacks registered after download completes if the download fails', inject(function(crewService, $httpBackend){
            var callback = jasmine.createSpy('callback');
            $httpBackend.when('GET', 'data/crew/names.json').respond(404, null);
            
            $httpBackend.flush();            
            crewService.whenLoaded(callback);
            
            expect(callback).not.toHaveBeenCalled();
        }));          
    });
    describe('tick', function(){
        it('approaches the tile the crewmember should move towards', inject(function(crewService, entityService){
            spyOn(entityService, 'approach').and.returnValue(true);
            
            crewService.tick('Ship', {
                displayLocation: 'Display Location', 
                targetLocation: 'Target Location'
            });
            
            expect(entityService.approach).toHaveBeenCalledWith('Display Location', 'Target Location');
        }));
        describe('when we have reached the tile the crewmember should move towards', function(){
            describe('when we are not in the room the crewmember is attempting to access', function(){
                it('finds the matching door in the room we are moving into and moves into its tile', inject(function(crewService, entityService, pathfindingService){
                    spyOn(entityService, 'approach').and.returnValue(false);
                    spyOn(pathfindingService, 'route').and.callFake(function(shipModel, entityModel){});
                    
                    var shipModel = {
                        rooms: [{
                            
                        }, {
                            // The room we are entering.
                            doors: [{
                                doorId: 6
                            }, {
                                doorId: 8,
                                x: 8,
                                y: 6
                            }, {
                                doorId: 9
                            }]                            
                        }, {
                            // The room we are leaving.
                            doors: [{
                                
                            }, {
                                
                            }, {
                                
                            }, {
                                
                            }, {
                                doorId: 8,
                                roomId: 1
                            }, {
                                
                            }]                            
                        }]
                    };
                    var crewModel = {
                        roomId: 8,
                        targetLocation: {
                            doorLookupId: 4
                        }, 
                        displayLocation: {
                            roomId: 2
                        }
                    };
                    
                    crewService.tick(shipModel, crewModel);

                    expect(crewModel.displayLocation.roomId).toEqual(1);
                    expect(crewModel.displayLocation.x).toEqual(8);
                    expect(crewModel.displayLocation.y).toEqual(6);                    
                }));
                it('refreshes the pathfinding route', inject(function(crewService, entityService, pathfindingService){
                    spyOn(entityService, 'approach').and.returnValue(false);
                    spyOn(pathfindingService, 'route');
                    
                    var shipModel = {
                        rooms: [{
                            
                        }, {
                            // The room we are entering.
                            doors: [{
                                doorId: 6
                            }, {
                                doorId: 8,
                            }, {
                                doorId: 9
                            }]                            
                        }, {
                            // The room we are leaving.
                            doors: [{
                                
                            }, {
                                
                            }, {
                                
                            }, {
                                
                            }, {
                                doorId: 8,
                                roomId: 1
                            }, {
                                
                            }]                            
                        }]
                    };
                    var crewModel = {
                        roomId: 8,
                        targetLocation: {
                            doorLookupId: 4
                        }, 
                        displayLocation: {
                            roomId: 2
                        }
                    };
                    
                    crewService.tick(shipModel, crewModel);
                    
                    expect(pathfindingService.route).toHaveBeenCalledWith(shipModel, crewModel);
                }));
            });
            describe('when we are in the room the crewmember is attempting to access', function(){
                it('does nothing', inject(function(crewService, entityService, pathfindingService){
                    spyOn(entityService, 'approach').and.returnValue(false);
                    spyOn(pathfindingService, 'route');
                    
                    var shipModel = {};
                    var crewModel = {
                        roomId: 3,
                        displayLocation: {
                            roomId: 3
                        }
                    };
                    
                    crewService.tick(shipModel, crewModel);
                    
                    expect(pathfindingService.route).not.toHaveBeenCalledWith(shipModel, crewModel);                    
                }));
            });
        });
    });
});