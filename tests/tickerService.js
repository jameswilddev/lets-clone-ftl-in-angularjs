describe('tickerService', function(){
    beforeEach(module('tickerService'));
    describe('initial state', function(){
        it('has an empty toTick array', inject(function(tickerService){
            expect(tickerService.toTick).toEqual([]);
        }));
        it('is paused', inject(function(tickerService){
            expect(tickerService.paused()).toBeTruthy();
        }));
        it('does not call any functions in toTick yet', inject(function(tickerService, $interval){
            var spyA = jasmine.createSpy();
            tickerService.toTick.push(spyA);
            $interval.flush(1000);
            expect(spyA).not.toHaveBeenCalled();
        }));
        describe('pause', function(){
            it('does not call any functions in toTick yet', inject(function(tickerService, $interval){
                var spyA = jasmine.createSpy();
                tickerService.toTick.push(spyA);
                $interval.flush(1000);
            }));
            it('is paused', inject(function(tickerService){
                expect(tickerService.paused()).toBeTruthy();
            }));
        });
        describe('togglePause', function(){
            it('is not paused', inject(function(tickerService){
                tickerService.togglePause();
                expect(tickerService.paused()).toBeFalsy();
            }));
            it('does not call any functions immediately', inject(function(tickerService){
                var spyA = jasmine.createSpy();
                tickerService.toTick.push(spyA);
                tickerService.togglePause();
                expect(spyA).not.toHaveBeenCalled();
            }));
            it('does not call any functions before the first interval', inject(function(tickerService, $interval){
                var spyA = jasmine.createSpy();
                tickerService.toTick.push(spyA);
                tickerService.togglePause();
                $interval.flush(240);
                expect(spyA).not.toHaveBeenCalled();
            }));            
            it('calls all functions in toTick after the first interval', inject(function(tickerService, $interval){
                var spyA = jasmine.createSpy();
                tickerService.toTick.push(spyA);
                var spyB = jasmine.createSpy();
                tickerService.toTick.push(spyB);
                tickerService.togglePause();
                $interval.flush(260);
                expect(spyA.calls.count()).toEqual(1);
                expect(spyB.calls.count()).toEqual(1);
            }));     
            it('accepts functions added to toTick after unpausing', inject(function(tickerService, $interval){
                tickerService.togglePause();
                var spyA = jasmine.createSpy();
                tickerService.toTick.push(spyA);
                var spyB = jasmine.createSpy();
                tickerService.toTick.push(spyB);                
                $interval.flush(260);
                expect(spyA.calls.count()).toEqual(1);
                expect(spyB.calls.count()).toEqual(1);
            }));             
            it('calls all functions in toTick after the second interval', inject(function(tickerService, $interval){
                var spyA = jasmine.createSpy();
                tickerService.toTick.push(spyA);
                var spyB = jasmine.createSpy();
                tickerService.toTick.push(spyB);
                tickerService.togglePause();
                $interval.flush(510);
                expect(spyA.calls.count()).toEqual(2);
                expect(spyB.calls.count()).toEqual(2);
            }));
            describe('togglePause', function(){
                it('is paused', inject(function(tickerService){
                    tickerService.togglePause();
                    tickerService.togglePause();                    
                    expect(tickerService.paused()).toBeTruthy();
                }));
                it('does not call any functions in toTick', inject(function(tickerService, $interval){
                    var spyA = jasmine.createSpy();
                    tickerService.toTick.push(spyA);
                    tickerService.togglePause();
                    tickerService.togglePause();                                        
                    $interval.flush(1000);
                    expect(spyA).not.toHaveBeenCalled();
                }));
            });
            describe('pause', function(){
                it('is paused', inject(function(tickerService){
                    tickerService.togglePause();
                    tickerService.pause();                    
                    expect(tickerService.paused()).toBeTruthy();
                }));
                it('does not call any functions in toTick', inject(function(tickerService, $interval){
                    var spyA = jasmine.createSpy();
                    tickerService.toTick.push(spyA);
                    tickerService.togglePause();
                    tickerService.pause();                                        
                    $interval.flush(1000);
                    expect(spyA).not.toHaveBeenCalled();
                }));
            });            
        });
        describe('unpause', function(){
            it('is not paused', inject(function(tickerService){
                tickerService.unpause();
                expect(tickerService.paused()).toBeFalsy();
            }));
            it('does not call any functions immediately', inject(function(tickerService){
                var spyA = jasmine.createSpy();
                tickerService.toTick.push(spyA);
                tickerService.unpause();
                expect(spyA).not.toHaveBeenCalled();
            }));
            it('does not call any functions before the first interval', inject(function(tickerService, $interval){
                var spyA = jasmine.createSpy();
                tickerService.toTick.push(spyA);
                tickerService.unpause();
                $interval.flush(240);
                expect(spyA).not.toHaveBeenCalled();
            }));            
            it('calls all functions in toTick after the first interval', inject(function(tickerService, $interval){
                var spyA = jasmine.createSpy();
                tickerService.toTick.push(spyA);
                var spyB = jasmine.createSpy();
                tickerService.toTick.push(spyB);
                tickerService.unpause();
                $interval.flush(260);
                expect(spyA.calls.count()).toEqual(1);
                expect(spyB.calls.count()).toEqual(1);
            }));     
            it('accepts functions added to toTick after unpausing', inject(function(tickerService, $interval){
                tickerService.unpause();
                var spyA = jasmine.createSpy();
                tickerService.toTick.push(spyA);
                var spyB = jasmine.createSpy();
                tickerService.toTick.push(spyB);                
                $interval.flush(260);
                expect(spyA.calls.count()).toEqual(1);
                expect(spyB.calls.count()).toEqual(1);
            }));             
            it('calls all functions in toTick after the second interval', inject(function(tickerService, $interval){
                var spyA = jasmine.createSpy();
                tickerService.toTick.push(spyA);
                var spyB = jasmine.createSpy();
                tickerService.toTick.push(spyB);
                tickerService.unpause();
                $interval.flush(510);
                expect(spyA.calls.count()).toEqual(2);
                expect(spyB.calls.count()).toEqual(2);
            }));
            describe('togglePause', function(){
                it('is paused', inject(function(tickerService){
                    tickerService.unpause();
                    tickerService.togglePause();
                    expect(tickerService.paused()).toBeTruthy();
                }));
                it('does not call any functions in toTick', inject(function(tickerService, $interval){
                    var spyA = jasmine.createSpy();
                    tickerService.toTick.push(spyA);
                    tickerService.unpause();
                    tickerService.togglePause();                    
                    $interval.flush(1000);
                    expect(spyA).not.toHaveBeenCalled();
                }));
            });
            describe('togglePause', function(){
                it('is paused', inject(function(tickerService){
                    tickerService.unpause();
                    tickerService.pause();
                    expect(tickerService.paused()).toBeTruthy();
                }));
                it('does not call any functions in toTick', inject(function(tickerService, $interval){
                    var spyA = jasmine.createSpy();
                    tickerService.toTick.push(spyA);
                    tickerService.unpause();
                    tickerService.pause();                    
                    $interval.flush(1000);
                    expect(spyA).not.toHaveBeenCalled();
                }));
            });            
        });        
    });
});