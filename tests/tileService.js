describe('tileService', function(){
    beforeEach(module('tileService'));
    describe('occupied', function(){
        var shipModel;
        beforeEach(function(){
            shipModel = {
                fires: [],
                crew: [],
                intruders: []
            };
        });
        it('returns false when crew or intruders are on other tiles', inject(function(tileService){
            shipModel.crew.push({x: 5, y: 7});
            shipModel.intruders.push({x: 4, y: 8});
            
            expect(tileService.occupied(shipModel, 4, 7)).toBeFalsy();
        }));           
        it('returns false when fire occupies the tile', inject(function(tileService){
            shipModel.fires.push({x: 4, y: 7});
            
            expect(tileService.occupied(shipModel, 4, 7)).toBeFalsy();
        }));        
        it('returns true when a crewmember occupies the tile', inject(function(tileService){
            shipModel.crew.push({x: 4, y: 7});
            
            expect(tileService.occupied(shipModel, 4, 7)).toBeTruthy();
        }));  
        it('returns true when an intruder occupies the tile', inject(function(tileService){
            shipModel.intruders.push({x: 4, y: 7});
            
            expect(tileService.occupied(shipModel, 4, 7)).toBeTruthy();
        }));          
    });
});