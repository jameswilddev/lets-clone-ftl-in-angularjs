describe('doorService', function(){
    beforeEach(module('doorService'));
    describe('import', function() {
        it('copies x, y and vertical to the model from the template', inject(function(doorService){
            var shipTemplate = {
                rooms: []
            };
            var doorTemplate = {
                x: 5,
                y: -7,
                vertical: true
            };
            
            var doorModel = doorService.import(shipTemplate, doorTemplate);
            
            expect(doorModel.x).toEqual(5);
            expect(doorModel.y).toEqual(-7);
            expect(doorModel.vertical).toBeTruthy();
        }));
    });
    describe('getOtherRoomId', function(){
        it('returns undefined if the other room is undefined', inject(function(doorService){
            var doorModel = {
                fromRoomId: 60
            };
            
            var otherRoomId = doorService.getOtherRoomId(doorModel, 60);
            
            expect(otherRoomId).toBeUndefined();
        }));
        
        it('returns toRoomId if we give fromRoomId', inject(function(doorService){
            var doorModel = {
                fromRoomId: 60,
                toRoomId: 70
            };
            
            var otherRoomId = doorService.getOtherRoomId(doorModel, 60);
            
            expect(otherRoomId).toEqual(70);
        }));        
        it('returns toRoomId if we give fromRoomId', inject(function(doorService){
            var doorModel = {
                fromRoomId: 60,
                toRoomId: 70
            };
            
            var otherRoomId = doorService.getOtherRoomId(doorModel, 70);
            
            expect(otherRoomId).toEqual(60);
        }));                
    });
    describe('iterateConnectingRooms', function(){
        it('executes the callback for each room linked to the specified room by a door', inject(function(doorService){
            var shipModel = {
                doors: [{
                    fromRoomId: 3,
                    toRoomId: 7
                }, {
                    fromRoomId: 4,
                    toRoomId: 3
                }]
            };
            var callback = jasmine.createSpy('callback');
            
            doorService.iterateConnectingRooms(shipModel, 3, callback);
            
            expect(callback).toHaveBeenCalledWith(7);
            expect(callback).toHaveBeenCalledWith(4);
        }));
        // We had a bug wherein doors to roomId zero were being ignored.
        it('executes the callback for doors to room zero', inject(function(doorService){
            var shipModel = {
                doors: [{
                    fromRoomId: 0,
                    toRoomId: 7
                }, {
                    fromRoomId: 4,
                    toRoomId: 0
                }]
            };
            var callback = jasmine.createSpy('callback');
            
            doorService.iterateConnectingRooms(shipModel, 0, callback);
            
            expect(callback).toHaveBeenCalledWith(7);
            expect(callback).toHaveBeenCalledWith(4);
        }));        
        it('ignores doors to space', inject(function(doorService){
            var shipModel = {
                doors: [{
                    fromRoomId: 3,
                    toRoomId: undefined
                }]
            };
            var callback = jasmine.createSpy('callback');
            
            doorService.iterateConnectingRooms(shipModel, 3, callback);
            
            expect(callback).not.toHaveBeenCalled();        
        }));
        it('ignores doors which do not connect to the specified room', inject(function(doorService){
            var shipModel = {
                doors: [{
                    fromRoomId: 4,
                    toRoomId: 7
                }]
            };
            var callback = jasmine.createSpy('callback');
            
            doorService.iterateConnectingRooms(shipModel, 3, callback);
            
            expect(callback).not.toHaveBeenCalled();                 
        }));
    });   
    describe('roomMatching', function(){
        it('defines distinct constants for the left/right/top/bottom borders', inject(function(doorService){
            expect(doorService.roomMatching).toBeDefined();
            expect(doorService.roomMatching.left).toBeDefined();
            expect(doorService.roomMatching.right).toBeDefined();
            expect(doorService.roomMatching.top).toBeDefined();
            expect(doorService.roomMatching.bottom).toBeDefined();
            expect(doorService.roomMatching.left).not.toBe(doorService.roomMatching.right);
            expect(doorService.roomMatching.left).not.toBe(doorService.roomMatching.top);
            expect(doorService.roomMatching.left).not.toBe(doorService.roomMatching.bottom);
            expect(doorService.roomMatching.right).not.toBe(doorService.roomMatching.top);
            expect(doorService.roomMatching.right).not.toBe(doorService.roomMatching.bottom);
            expect(doorService.roomMatching.top).not.toBe(doorService.roomMatching.bottom);
        }));
        describe('match', function(){
            var roomTemplate = {
                left: 4,
                right: 8, 
                bottom: 12,
                top: 15
            };
            var doorTemplate;
            beforeEach(function(){
                doorTemplate = {};
            });
            describe('when horizontal', function(){
                beforeEach(function(){
                    doorTemplate.vertical = false;
                });
                describe('when beyond the top edge', function(){
                    beforeEach(function(){
                        doorTemplate.y = 16;
                    });
                    it('returns undefined when between the left and right corners', inject(function(doorService){
                        doorTemplate.x = 6;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));
                });                
                describe('when on the top edge', function(){
                    beforeEach(function(){
                        doorTemplate.y = 15;
                    });
                    it('returns top when between the left and right corners', inject(function(doorService){
                        doorTemplate.x = 6;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBe(doorService.roomMatching.top);
                    }));
                    it('returns top when in the left corner', inject(function(doorService){
                        doorTemplate.x = 4;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBe(doorService.roomMatching.top);
                    }));   
                    it('returns top when in the right corner', inject(function(doorService){
                        doorTemplate.x = 7;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBe(doorService.roomMatching.top);
                    }));   
                    it('returns undefined when beyond the left corner', inject(function(doorService){
                        doorTemplate.x = 3;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));    
                    it('returns undefined when beyond the right corner', inject(function(doorService){
                        doorTemplate.x = 8;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));                      
                });
                describe('when between the top and bottom edges', function(){
                    beforeEach(function(){
                        doorTemplate.y = 13;
                    });
                    it('returns undefined when between the left and right corners', inject(function(doorService){
                        doorTemplate.x = 6;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));
                });  
                describe('when on the bottom edge', function(){
                    beforeEach(function(){
                        doorTemplate.y = 12;
                    });
                    it('returns bottom when between the left and right corners', inject(function(doorService){
                        doorTemplate.x = 6;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBe(doorService.roomMatching.bottom);
                    }));
                    it('returns bottom when in the left corner', inject(function(doorService){
                        doorTemplate.x = 4;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBe(doorService.roomMatching.bottom);
                    }));   
                    it('returns top when in the right corner', inject(function(doorService){
                        doorTemplate.x = 7;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBe(doorService.roomMatching.bottom);
                    }));   
                    it('returns undefined when beyond the left corner', inject(function(doorService){
                        doorTemplate.x = 3;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));    
                    it('returns undefined when beyond the right corner', inject(function(doorService){
                        doorTemplate.x = 8;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));                      
                });   
                describe('when beyond the bottom edge', function(){
                    beforeEach(function(){
                        doorTemplate.y = 11;
                    });
                    it('returns undefined when between the left and right corners', inject(function(doorService){
                        doorTemplate.x = 6;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));
                });                   
            });
            describe('when vertical', function(){
                beforeEach(function(){
                    doorTemplate.vertical = true;
                });
                describe('when beyond the left edge', function(){
                    beforeEach(function(){
                        doorTemplate.x = 3;
                    });
                    it('returns undefined when between the top and bottom corners', inject(function(doorService){
                        doorTemplate.x = 13;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));
                });                
                describe('when on the left edge', function(){
                    beforeEach(function(){
                        doorTemplate.x = 4;
                    });
                    it('returns left when between the top and bottom corners', inject(function(doorService){
                        doorTemplate.y = 13;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBe(doorService.roomMatching.left);
                    }));
                    it('returns left when in the top corner', inject(function(doorService){
                        doorTemplate.y = 14;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBe(doorService.roomMatching.left);
                    }));   
                    it('returns left when in the bottom corner', inject(function(doorService){
                        doorTemplate.y = 12;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBe(doorService.roomMatching.left);
                    }));   
                    it('returns undefined when beyond the top corner', inject(function(doorService){
                        doorTemplate.y = 15;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));    
                    it('returns undefined when beyond the bottom corner', inject(function(doorService){
                        doorTemplate.x = 11;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));                      
                });
                describe('when between the top and bottom edges', function(){
                    beforeEach(function(){
                        doorTemplate.x = 3;
                    });
                    it('returns undefined when between the top and bottom corners', inject(function(doorService){
                        doorTemplate.y = 13;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));
                });  
                describe('when on the right edge', function(){
                    beforeEach(function(){
                        doorTemplate.x = 8;
                    });
                    it('returns right when between the top and bottom corners', inject(function(doorService){
                        doorTemplate.y = 13;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBe(doorService.roomMatching.right);
                    }));
                    it('returns right when in the top corner', inject(function(doorService){
                        doorTemplate.y = 14;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBe(doorService.roomMatching.right);
                    }));   
                    it('returns right when in the bottom corner', inject(function(doorService){
                        doorTemplate.y = 12;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBe(doorService.roomMatching.right);
                    }));   
                    it('returns undefined when beyond the top corner', inject(function(doorService){
                        doorTemplate.y = 15;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));    
                    it('returns undefined when beyond the bottom corner', inject(function(doorService){
                        doorTemplate.x = 11;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));                     
                });   
                describe('when beyond the right edge', function(){
                    beforeEach(function(){
                        doorTemplate.x = 9;
                    });
                    it('returns undefined when between the top and bottom corners', inject(function(doorService){
                        doorTemplate.y = 13;
                        
                        var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                        
                        expect(match).toBeUndefined();
                    }));
                });                   
            });            
        });
    });
});