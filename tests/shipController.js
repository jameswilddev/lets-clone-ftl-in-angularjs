describe('shipController', function(){
    var scope;
    beforeEach(function(){
        module('shipController');
        inject(function($rootScope, $controller){
            scope = $rootScope.$new();
            $controller('shipController', {$scope: scope});
        });
    });
    describe('init', function(){
        it('copies the relevant properties from the shipModel into the scope', function(){
            rooms = {};
            doors = {};
            crew = {};
            intruders = {};
            fires = {};
            
            scope.init({
                rooms: rooms,
                doors: doors,
                crew: crew,
                intruders: intruders,
                fires: fires
            });
            
            expect(scope.rooms).toBe(rooms);
            expect(scope.doors).toBe(doors);
            expect(scope.crew).toBe(crew);
            expect(scope.intruders).toBe(intruders);
            expect(scope.fires).toBe(fires);
        });
        it('registers a callback with the tickerService', inject(function(tickerService){
            scope.init({});
            
            expect(tickerService.toTick.length).toEqual(1);
        }));
        it('calls shipService.tick when executing the callback', inject(function(tickerService, shipService){
            spyOn(shipService, 'tick');
            
            scope.init('Test Ship Model');
            tickerService.toTick[0]();
            
            expect(shipService.tick).toHaveBeenCalledWith('Test Ship Model');
        }));
    });
    describe('click', function(){
        var shipModel;
        beforeEach(function(){
            shipModel = {
                rooms: ['Room A', 'Room B', 'Room C'],
                crew: [{
                    selected: false,
                    roomId: 2
                }, {
                    selected: true,
                    roomId: 2
                }, {
                    selected: true,
                    roomId: 2
                }]
            };
            scope.init(shipModel);
        });
        it('transfers selected crew members to the target room', inject(function(roomService, pathfindingService){
            spyOn(roomService, 'findEmptyTile').and.callFake(function(shipModelArg, roomModelArg, allowDangerousTilesArg, outputArg){
                expect(shipModelArg).toBe(shipModel);
                expect(roomModelArg).toEqual('Room B');
                expect(allowDangerousTilesArg).toBeFalsy();
                expect(outputArg.passedAsOutput).toBeUndefined();
                outputArg.passedAsOutput = true;
                if(outputArg === shipModel.crew[1]) return true;
                if(outputArg === shipModel.crew[2]) return true;
                expect(false).toBeTruthy();
            });
            spyOn(pathfindingService, 'route').and.callFake(function(shipModelArg, crewModelArg){
                expect(shipModelArg).toBe(shipModel);
                expect(crewModelArg.routed).toBeUndefined();
                crewModelArg.routed = true;
                if(crewModelArg === shipModel.crew[1]) return true;
                if(crewModelArg === shipModel.crew[2]) return true;
                expect(false).toBeTruthy();
            });
            
            scope.roomClick(shipModel.rooms[1]);
            
            expect(shipModel.crew).toEqual([{
                selected: false,
                roomId: 2
            }, {
                selected: true,
                roomId: 1,
                passedAsOutput: true,
                routed: true
            }, {
                selected: true,
                roomId: 1,
                passedAsOutput: true,
                routed: true
            }]);
        }));
        it('does not move crew members who are already in the room', inject(function(roomService, pathfindingService){
            shipModel.crew[1].roomId = 1;
            spyOn(roomService, 'findEmptyTile').and.callFake(function(shipModelArg, roomModelArg, allowDangerousTilesArg, outputArg){
                expect(shipModelArg).toBe(shipModel);
                expect(roomModelArg).toEqual('Room B');
                expect(allowDangerousTilesArg).toBeFalsy();
                expect(outputArg.passedAsOutput).toBeUndefined();
                outputArg.passedAsOutput = true;
                if(outputArg === shipModel.crew[2]) return true;
                expect(false).toBeTruthy();
            });
            spyOn(pathfindingService, 'route').and.callFake(function(shipModelArg, crewModelArg){
                expect(shipModelArg).toBe(shipModel);
                expect(crewModelArg.routed).toBeUndefined();
                crewModelArg.routed = true;
                if(crewModelArg === shipModel.crew[2]) return true;
                expect(false).toBeTruthy();
            });            
            
            scope.roomClick(shipModel.rooms[1]);
            
            expect(shipModel.crew).toEqual([{
                selected: false,
                roomId: 2
            }, {
                selected: true,
                roomId: 1
            }, {
                selected: true,
                roomId: 1,
                passedAsOutput: true,
                routed: true
            }]);         
        }));
        it('will fall back to dangerous tiles when no non-dangerous tiles are left', inject(function(roomService, pathfindingService){
            spyOn(roomService, 'findEmptyTile').and.callFake(function(shipModelArg, roomModelArg, allowDangerousTilesArg, outputArg){
                expect(shipModelArg).toBe(shipModel);
                expect(roomModelArg).toEqual('Room B');
                if(allowDangerousTilesArg) {
                    expect(outputArg).toBe(shipModel.crew[2]);
                    expect(outputArg.passedAsOutputDangerous).toBeUndefined();
                    outputArg.passedAsOutputDangerous = true;
                } else {
                    expect(outputArg.passedAsOutput).toBeUndefined();
                    outputArg.passedAsOutput = true;
                }
                if(outputArg === shipModel.crew[1]) return true;
                if(outputArg === shipModel.crew[2]) return allowDangerousTilesArg;
                expect(false).toBeTruthy();
            });
            spyOn(pathfindingService, 'route').and.callFake(function(shipModelArg, crewModelArg){
                expect(shipModelArg).toBe(shipModel);
                expect(crewModelArg.routed).toBeUndefined();
                crewModelArg.routed = true;
                if(crewModelArg === shipModel.crew[1]) return true;
                if(crewModelArg === shipModel.crew[2]) return true;
                expect(false).toBeTruthy();
            });            
            
            scope.roomClick(shipModel.rooms[1]);
            
            expect(shipModel.crew).toEqual([{
                selected: false,
                roomId: 2
            }, {
                selected: true,
                roomId: 1,
                passedAsOutput: true,
                routed: true
            }, {
                selected: true,
                roomId: 1,
                passedAsOutput: true,
                passedAsOutputDangerous: true,
                routed: true
            }]);         
        }));
        it('will leave crew in their current location for which there is no room', inject(function(roomService, pathfindingService){
            spyOn(roomService, 'findEmptyTile').and.callFake(function(shipModelArg, roomModelArg, allowDangerousTilesArg, outputArg){
                expect(shipModelArg).toBe(shipModel);
                expect(roomModelArg).toEqual('Room B');
                if(allowDangerousTilesArg) {
                    expect(outputArg).toBe(shipModel.crew[2]);
                    expect(outputArg.passedAsOutputDangerous).toBeUndefined();
                    outputArg.passedAsOutputDangerous = true;
                } else {
                    expect(outputArg.passedAsOutput).toBeUndefined();
                    outputArg.passedAsOutput = true;
                }
                if(outputArg === shipModel.crew[1]) return true;
                if(outputArg === shipModel.crew[2]) return false;
                expect(false).toBeTruthy();
            });
            
            spyOn(pathfindingService, 'route').and.callFake(function(shipModelArg, crewModelArg){
                expect(shipModelArg).toBe(shipModel);
                expect(crewModelArg.routed).toBeUndefined();
                crewModelArg.routed = true;
                if(crewModelArg === shipModel.crew[1]) return true;
                expect(false).toBeTruthy();
            });            
            
            scope.roomClick(shipModel.rooms[1]);
            
            expect(shipModel.crew).toEqual([{
                selected: false,
                roomId: 2
            }, {
                selected: true,
                passedAsOutput: true,
                routed: true,
                roomId: 1
            }, {
                selected: true,
                roomId: 2,
                passedAsOutput: true,
                passedAsOutputDangerous: true
            }]);         
        }));
    });    
});