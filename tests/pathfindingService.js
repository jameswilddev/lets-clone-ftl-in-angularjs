describe('pathfindingService', function(){
    beforeEach(module('pathfindingService'));
    describe('route', function(){
        it('sets the target to the allocated location when the entity is in the same room', inject(function(pathfindingService){
            var entityModel = {
                x: 4,
                y: 7,
                roomId: 3,
                targetLocation: {
                    x: 9,
                    y: 10
                },
                displayLocation: {
                    x: 3,
                    y: 8,
                    roomId: 3
                }
            };
            
            pathfindingService.route({}, entityModel);
            
            expect(entityModel).toEqual({
                x: 4,
                y: 7,
                roomId: 3,
                targetLocation: {
                    x: 4,
                    y: 7
                },
                displayLocation: {
                    x: 3,
                    y: 8,
                    roomId: 3
                }
            });
        }));
        it('can resolve a basic, linear path', inject(function(pathfindingService){
            var shipModel = {
                rooms: ['Room A', 'Room B', 'Room C']
            };
            /*
            var entityModel = {
                roomId: 2,
                displayLocation
            };
            */
            //todo
        }));
        it('can resolve a basic, linear path going the other way', inject(function(pathfindingService){
            //todo
        }));
        it('ignores branches which don\'t go the right way', inject(function(pathfindingService){
            //todo
        }));
        it('takes the shortest route given multiple solutions', inject(function(pathfindingService){
            //todo
        }));
        it('takes the least dangerous route even if the dangerous route is shorter', inject(function(pathfindingService){
            //todo
        }));
    });
});