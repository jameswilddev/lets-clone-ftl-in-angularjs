describe('roomService', function(){
    beforeEach(module('roomService'));
    describe('import', function(){
        it('copies the room location', inject(function(roomService){
            var roomModel = roomService.import({
                doors: []
            }, {
                left: -3,
                bottom: -1,
                right: 2,
                top: 4
            });
            
            expect(roomModel.left).toEqual(-3);
            expect(roomModel.bottom).toEqual(-1);
            expect(roomModel.right).toEqual(2);
            expect(roomModel.top).toEqual(4);
        }));
        describe('creates a local lookup of doors which intersect it', function() {
            it('includes intersecting doors', inject(function(roomService, doorService){
                // Importing room B.
                // Door A connects rooms A and B and should be cached in the lookup.
                // Door B connects room B to vacuum and should be cached in the lookup.
                // Door C connects rooms A and C and has nothing to do with room B.
                // Door D connects rooms B and C and should be cached in the lookup.
                var shipTemplate = {
                    doors: ['Door A', 'Door B', 'Door C', 'Door D'],
                    rooms: ['Room A', 'Room B', 'Room C']
                };
                
                spyOn(doorService.roomMatching, 'match').and.callFake(function(roomTemplate, doorTemplate){
                    switch(roomTemplate){
                        case 'Room A':
                            switch(doorTemplate){
                                case 'Door A':
                                case 'Door C':
                                    return true;
                                    
                                case 'Door B':
                                case 'Door D':
                                    return false;
                                    
                                default:
                                    expect(false).toBeTruthy();
                                    break;
                            }
                            break;
                            
                        case 'Room B':
                            switch(doorTemplate){
                                case 'Door A':
                                case 'Door B':
                                case 'Door D':
                                    return true;
                                    
                                case 'Door C':
                                    return false;
                                    
                                default:
                                    expect(false).toBeTruthy();
                                    break;
                            }
                            break; 
                            
                        case 'Room C':
                            switch(doorTemplate){
                                case 'Door C':
                                case 'Door D':
                                    return true;
                                    
                                case 'Door A':
                                case 'Door B':
                                    return false;
                                    
                                default:
                                    expect(false).toBeTruthy();
                                    break;
                            }
                            break;                        
                            
                        default:
                            expect(false).toBeTruthy();
                            break;                        
                    }
                });
                
                var roomModel = roomService.import(shipTemplate, 'Room B');
                
                expect(roomModel.doors).toEqual([{
                    doorId: 0,
                    roomId: 0,
                    x: undefined,
                    y: undefined
                }, {
                    doorId: 1,
                    x: undefined,
                    y: undefined                
                }, {
                    doorId: 3,
                    roomId: 2,
                    x: undefined,
                    y: undefined                
                }]);
            }));
            it('finds the tile right of doors on the left edge', inject(function(roomService, doorService){
                var shipTemplate = {
                    doors: [{
                        x: 4,
                        y: 7
                    }],
                    rooms: [{}]
                };
                
                spyOn(doorService.roomMatching, 'match').and.returnValue(doorService.roomMatching.left);
                
                var roomModel = roomService.import(shipTemplate, shipTemplate.rooms[0]);
                
                expect(roomModel.doors).toEqual([{
                    doorId: 0,
                    x: 4,
                    y: 7
                }]);
            }));
            it('finds the tile left of doors on the right edge', inject(function(roomService, doorService){
                var shipTemplate = {
                    doors: [{
                        x: 4,
                        y: 7
                    }],
                    rooms: [{}]
                };
                
                spyOn(doorService.roomMatching, 'match').and.returnValue(doorService.roomMatching.right);
                
                var roomModel = roomService.import(shipTemplate, shipTemplate.rooms[0]);
                
                expect(roomModel.doors).toEqual([{
                    doorId: 0,
                    x: 3,
                    y: 7
                }]);
            }));   
            it('finds the tile above doors on the bottom edge', inject(function(roomService, doorService){
                var shipTemplate = {
                    doors: [{
                        x: 4,
                        y: 7
                    }],
                    rooms: [{}]
                };
                
                spyOn(doorService.roomMatching, 'match').and.returnValue(doorService.roomMatching.bottom);
                
                var roomModel = roomService.import(shipTemplate, shipTemplate.rooms[0]);
                
                expect(roomModel.doors).toEqual([{
                    doorId: 0,
                    x: 4,
                    y: 7
                }]);
            }));
            it('finds the tile below doors on the top edge', inject(function(roomService, doorService){
                var shipTemplate = {
                    doors: [{
                        x: 4,
                        y: 7
                    }],
                    rooms: [{}]
                };
                
                spyOn(doorService.roomMatching, 'match').and.returnValue(doorService.roomMatching.top);
                
                var roomModel = roomService.import(shipTemplate, shipTemplate.rooms[0]);
                
                expect(roomModel.doors).toEqual([{
                    doorId: 0,
                    x: 4,
                    y: 6
                }]);
            }));             
        });
    });
    describe('dangerous', function(){
        var roomModel = {
            left: 4,
            right: 7, 
            bottom: 2, 
            top: 8
        };        
        var shipModel;
        beforeEach(function(){
            shipModel = {
                crew: [],
                intruders: [],
                fires: []
            };
        });
        it('returns false when there is nothing in the room', inject(function(roomService, tileService){
            shipModel.crew.push({x: 3, y: 3 });
            shipModel.intruders.push({x: 8, y: 7});
            shipModel.fires.push({x: 5, y: 1});

            expect(roomService.dangerous(shipModel, roomModel)).toBeFalsy();
        }));
        it('returns true when fires are in the room', inject(function(roomService, tileService){
            shipModel.fires.push({x: 6, y: 3});
            
            expect(roomService.dangerous(shipModel, roomModel)).toBeTruthy();
        }));
        it('returns true when intruders are in the room', inject(function(roomService, tileService){
            shipModel.intruders.push({x: 6, y: 3});
            
            expect(roomService.dangerous(shipModel, roomModel)).toBeTruthy();
        }));    
        it('returns false when crewmembers are in the room', inject(function(roomService, tileService){
            shipModel.crew.push({x: 6, y: 3});
            
            expect(roomService.dangerous(shipModel, roomModel)).toBeFalsy();
        }));          
    });
    describe('findEmptyTile', function(){
        var roomModel = {
            left: 2,
            right: 4,
            bottom: 3,
            top: 5
        };
        var shipModel, output;
        beforeEach((function(){
            shipModel = {
                crew: [],
                intruders: [],
                fires: []
            };
            output = {};
        }))
        describe('when allowing dangerous tiles', function(){
            it('finds a tile which is not occupied by a crewmember or intruder', inject(function(roomService, tileService){
                shipModel.crew.push({x: 2, y: 3});
                shipModel.intruders.push({x: 3, y: 3});
                
                expect(roomService.findEmptyTile(shipModel, roomModel, true, output)).toBeTruthy();
                    
                expect(output).toEqual({x: 2, y: 4});
            }));  
            it('will take the first unoccupied tile even if it is dangerous', inject(function(roomService, tileService){
                shipModel.crew.push({x: 2, y: 3});
                shipModel.intruders.push({x: 2, y: 4});
                shipModel.fires.push({x: 3, y: 3});
                
                expect(roomService.findEmptyTile(shipModel, roomModel, true, output)).toBeTruthy();
                    
                expect(output).toEqual({x: 3, y: 3});
            }));              
            it('returns false and does not modify output if all tiles are occupied', inject(function(roomService, tileService){
                shipModel.crew.push({x: 2, y: 3});
                shipModel.intruders.push({x: 3, y: 3});
                shipModel.intruders.push({x: 2, y: 4});
                shipModel.crew.push({x: 3, y: 4});
                
                expect(roomService.findEmptyTile(shipModel, roomModel, true, output)).toBeFalsy();
                    
                expect(output).toEqual({});
            }));              
        });
        describe('when disallowing dangerous tiles', function(){
            it('finds a tile which is not occupied by a crewmember or intruder', inject(function(roomService, tileService){
                shipModel.crew.push({x: 2, y: 3});
                shipModel.intruders.push({x: 3, y: 3});
                
                expect(roomService.findEmptyTile(shipModel, roomModel, false, output)).toBeTruthy();
                    
                expect(output).toEqual({x: 2, y: 4});
            }));  
            it('will skip dangerous tiles', inject(function(roomService, tileService){
                shipModel.crew.push({x: 2, y: 3});
                shipModel.intruders.push({x: 3, y: 3});
                shipModel.fires.push({x: 2, y: 4});
                
                expect(roomService.findEmptyTile(shipModel, roomModel, false, output)).toBeTruthy();
                    
                expect(output).toEqual({x: 3, y: 4});
            }));              
            it('returns false and does not modify output if all tiles are occupied', inject(function(roomService, tileService){
                shipModel.crew.push({x: 2, y: 3});
                shipModel.intruders.push({x: 3, y: 3});
                shipModel.intruders.push({x: 2, y: 4});
                shipModel.crew.push({x: 3, y: 4});
                
                expect(roomService.findEmptyTile(shipModel, roomModel, false, output)).toBeFalsy();
                    
                expect(output).toEqual({});
            }));  
            it('returns false and does not modify output if all tiles are dangerous', inject(function(roomService, tileService){
                shipModel.fires.push({x: 2, y: 3});
                shipModel.fires.push({x: 3, y: 3});
                shipModel.fires.push({x: 2, y: 4});
                shipModel.fires.push({x: 3, y: 4});
                
                expect(roomService.findEmptyTile(shipModel, roomModel, false, output)).toBeFalsy();
                    
                expect(output).toEqual({});
            }));              
        });
    });
});