describe('fireService', function(){
    beforeEach(module('fireService'));
    describe('create', function(){
        it('copies x and y to the fire model', inject(function(fireService){
            var model = fireService.create(4, -7);
            
            expect(model.x).toEqual(4);
            expect(model.y).toEqual(-7);
        }));
    });
   
});