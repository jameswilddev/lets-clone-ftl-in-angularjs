angular.module('roomService', ['tileService', 'boundsService', 'vectorService', 'doorService']).service('roomService', function(tileService, boundsService, vectorService, doorService){
    
    /** @namespace roomService */    
    var roomService = {
        /**
         * @memberOf roomService
         * @param {ShipTemplate} shipTemplate
         * @param {RoomTemplate} roomTemplate
         * @returns {RoomModel}
         */
        import: function(shipTemplate, roomTemplate){
            var roomModel = {
                doors: []
            };
            boundsService.clone(roomTemplate, roomModel);
            
            // Build local lookups for doors.
            angular.forEach(shipTemplate.doors, function(doorTemplate){
                var match = doorService.roomMatching.match(roomTemplate, doorTemplate);
                if(!match) return;
                var otherMatch;
                angular.forEach(shipTemplate.rooms, function(otherRoomTemplate){
                    if(roomTemplate == otherRoomTemplate) return;
                    if(!doorService.roomMatching.match(otherRoomTemplate, doorTemplate)) return;
                    otherMatch = otherRoomTemplate;
                });
                var doorLookupModel = {
                    doorId: shipTemplate.doors.indexOf(doorTemplate)
                };
                vectorService.clone(doorTemplate, doorLookupModel);
                switch(match){
                    case doorService.roomMatching.right:
                        doorLookupModel.x--;
                        break;
                    case doorService.roomMatching.top:
                        doorLookupModel.y--;
                        break;                        
                }
                if(otherMatch) doorLookupModel.roomId = shipTemplate.rooms.indexOf(otherMatch);
                roomModel.doors.push(doorLookupModel);
            });
            
            return roomModel;
        },
        
        /**
         * Determines whether the room is considered dangerous to occupants.
         * @memberOf roomService
         * @param {ShipModel} shipModel
         * @param {RoomModel} roomModel
         * @returns {Boolean}
         */
        dangerous: function(shipModel, roomModel){
            return boundsService.iterate(roomModel, function(x, y){
                if(tileService.dangerous(shipModel, x, y)) return true;
                return;
            });
        },
        
        /**
         * Finds an unoccupied tile and writes the coordinates to a given object.  
         * @memberOf roomService
         * @param {ShipModel} shipModel
         * @param {RoomModel} roomModel
         * @param {Boolean} allowDangerousTiles When false, the algorithm will ignore tiles
         * which are considered dangerous by tileService.dangerous.
         * @param {EntityModel} output If an empty tile is found, the x/y coordinates in
         * ship space are written to this object.
         * @returns {Boolean} True if an empty tile could be found, else false.
         */
        findEmptyTile: function(shipModel, roomModel, allowDangerousTiles, output){
            if(boundsService.iterate(roomModel, function(x, y){
                if(!allowDangerousTiles && tileService.dangerous(shipModel, x, y)) return;
                if(tileService.occupied(shipModel, x, y)) return;
                output.x = x;
                output.y = y;
                return true;
            })) return true;
            return false;
        }
    };
    
    return roomService;
    
});