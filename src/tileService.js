angular.module('tileService', ['entityService']).service('tileService', function(entityService){
    
    /** 
     * A service for managing tiles inside a room.
     * @name tileService
     * @namespace tileService
     */       
    var tileService = {};
    
    /**
     * Determines whether a given tile contains danger such as a fire or hostile.
     */
    tileService.dangerous = function(shipModel, x, y){
        if(entityService.findAt(shipModel.intruders, x, y)) return true;
        if(entityService.findAt(shipModel.fires, x, y)) return true;
        return false;
    };    
    
    /**
     * Determines whether a given tile is occupied; if false, it may be taken
     * by a crew member or hostile seeking to enter the room.
     */
    tileService.occupied = function(shipModel, x, y){
        if(entityService.findAt(shipModel.crew, x, y)) return true;
        if(entityService.findAt(shipModel.intruders, x, y)) return true;
        return false;
    };
    
    return tileService;
    
});
