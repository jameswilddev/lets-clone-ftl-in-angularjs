angular.module('shipController', ['pathfindingService', 'shipService', 'roomService', 'tickerService']).controller('shipController', function($scope, pathfindingService, shipService, roomService, tickerService){
    
    /**
     * Contains interaction logic for working with a ship.
     * @name shipController
     * @namespace
     */
    
    /**
     * Call after construction to load a ship.
     * @param {shipModel} shipModel The ship to load.
     */
    $scope.init = function(shipModel){
        var tick = function(){
            shipService.tick(shipModel);
        };
        
        tickerService.toTick.push(tick);
        
        // TODO: remove our tick function from the ticker service on
        // $scope.destroy
        
        /**
         * A list of the rooms in the ship.
         */
        $scope.rooms = shipModel.rooms;
        
        /**
         * A list of the doors in the ship.
         */        
        $scope.doors = shipModel.doors;
        
        /**
         * A list of the crewmembers in the ship.
         */        
        $scope.crew = shipModel.crew;
        
        /**
         * A list of the intruders in the ship.
         */        
        $scope.intruders = shipModel.intruders;   
        
        /**
         * A list of the fires in the ship.
         */        
        $scope.fires = shipModel.fires;           
        
        /**
         * To be called when a room is clicked on.
         * Currently sends the selected crew to the room, if space is available.
         * @param {roomModel} roomModel The room which was clicked.
         */
        $scope.roomClick = function(roomModel){
            var roomId = shipModel.rooms.indexOf(roomModel);
            angular.forEach(shipModel.crew, function(crewModel){
                if(!crewModel.selected) return;
                if(crewModel.roomId == roomId) return;
                if(!roomService.findEmptyTile(shipModel, roomModel, false, crewModel) 
                    && !roomService.findEmptyTile(shipModel, roomModel, true, crewModel)) 
                    return;
                crewModel.roomId = roomId;
                pathfindingService.route(shipModel, crewModel);
            });
        };        
    };
});