angular.module('crewService', ['random', 'doorService', 'pathfindingService', 'entityService', 'vectorService']).service('crewService', function($http, $window, random, $q, doorService, pathfindingService, entityService, vectorService){
    
    /** 
     * A service handling crew members in a ship.
     * Do not make calls before completing the whenLoaded callback.
     * @name crewService
     * @namespace crewService
     */         
    var crewService = {};
    
    var completedNames = false;
    
    var names = $http({
        url: 'data/crew/names.json',
        method: 'GET'
    }).success(function(data){
        completedNames = true;
        crewService.names = data;
    }).error(function(data){
        $window.alert('Failed to download the list of crew names.');
    });
    
    /**
     * Executes a function in a promise-style callback once the external files
     * required to use this service have been downloaded.
     */
    crewService.whenLoaded = function(callback){
        if(completedNames) {
            callback();
        } else {
            $q.all([names]).then(callback);
        }
    };
    
    /**
     * Creates and returns a new crew member.
     * Do not call without having first completing whenLoaded.
     */
    crewService.create = function(x, y, roomId){
        return {
            name: crewService.names[random.integer(0, crewService.names.length - 1)],
            x: x,
            y: y,
            roomId: roomId,
            displayLocation: {
                x: x,
                y: y,
                roomId: roomId
            },
            targetLocation: {
                x: x,
                y: y
            },
            selected: false
        };
    };
    
    /**
     * Updates a given crewmember.
     * To be called by shipService.
     */
    crewService.tick = function(shipModel, crewModel){
        // Try and walk where we'd like to go in the current room.
        // If we took a step, stop processing for this tick.
        if(entityService.approach(crewModel.displayLocation, crewModel.targetLocation)) return;
        // We've reached our destination in the current room, but the current
        // room isn't where we'd like to be overall.  Find the door that
        // connects this room with the next and use it.
        if(crewModel.displayLocation.roomId != crewModel.roomId){
            var doorLookupModel = shipModel.rooms[crewModel.displayLocation.roomId].doors[crewModel.targetLocation.doorLookupId];
            crewModel.displayLocation.roomId = doorLookupModel.roomId;
            angular.forEach(shipModel.rooms[doorLookupModel.roomId].doors, function(matchingDoorLookupModel){
                if(matchingDoorLookupModel.doorId != doorLookupModel.doorId) return;
                vectorService.clone(matchingDoorLookupModel, crewModel.displayLocation);
            });
            pathfindingService.route(shipModel, crewModel);
        }
    };
    
    return crewService;
})