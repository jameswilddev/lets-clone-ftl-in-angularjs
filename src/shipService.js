angular.module('shipService', ['roomService', 'reactorService', 'crewService', 'doorService']).service('shipService', function(roomService, reactorService, crewService, doorService){
    
    /** @namespace shipService */    
    var shipService = {   
    
        /**
         * @memberOf shipService
         * @param {ShipTemplate} shipTemplate
         * @returns {ShipModel} shipModel
         */
        import: function(shipTemplate){
            var shipModel = {
                rooms: [],
                crew: [], 
                intruders: [],
                fires: []
            };
            
            reactorService.initialize(shipModel);
            
            angular.forEach(shipTemplate.rooms, function(roomTemplate){
                shipModel.rooms.push(roomService.import(shipTemplate, roomTemplate));
            });
            
            shipModel.doors = [];
            
            angular.forEach(shipTemplate.doors, function(doorTemplate){
                shipModel.doors.push(doorService.import(shipTemplate, doorTemplate));
            });
        
            return shipModel;
        },
    
        /**
         * Finds an unoccupied tile and writes the coordinates to a given object.  
         * @memberOf shipService
         * @param {ShipModel} shipModel
         * @param {Boolean} allowDangerousTiles When false, the algorithm will ignore rooms
         * which are considered dangerous by roomService.dangerous.  Otherwise, the algorithm
         * will choose them as a last resort, but will prefer non-dangerous rooms.
         * @param {Boolean} allowDangerousTiles When false, the algorithm will ignore tiles
         * which are considered dangerous by tileService.dangerous.  Otherwise, the algorithm
         * will choose them as a last resort, but will prefer non-dangerous tiles.
         * @param {EntityModel} output If an empty tile is found, the x/y coordinates in
         * ship space are written to this object as well as roomId.
         * @returns {Boolean} True if an empty tile could be found, else false.
         */
        findEmptyTile: function(shipModel, allowDangerousRooms, allowDangerousTiles, output){
            var roomId, roomModel;
            // Find a tile that is not dangerous in a room that is not dangerous.
            for(roomId = 0; roomId < shipModel.rooms.length; roomId++){
                roomModel = shipModel.rooms[roomId];
                if(roomService.dangerous(shipModel, roomModel)) continue;
                if(!roomService.findEmptyTile(shipModel, roomModel, false, output)) continue;
                output.roomId = roomId;
                return true;
            }
            if(allowDangerousRooms){
                // Find a tile that is not dangerous in a room that is dangerous.
                for(roomId = 0; roomId < shipModel.rooms.length; roomId++){
                    roomModel = shipModel.rooms[roomId];
                    if(!roomService.findEmptyTile(shipModel, roomModel, false, output)) continue;
                    output.roomId = roomId;
                    return true;
                }        
                if(allowDangerousTiles){
                    // Find a tile that is dangerous in a room that is dangerous.
                    for(roomId = 0; roomId < shipModel.rooms.length; roomId++){
                        roomModel = shipModel.rooms[roomId];
                        if(!roomService.findEmptyTile(shipModel, roomModel, true, output)) continue;
                        output.roomId = roomId;
                        return true;
                    }      
                }
            }
            return false;
        },
        
        /**
         * Updates a given ship, also updating all models inside it.
         * To be called on an interval by tickerService.
         * @memberOf shipService
         */
        tick: function(shipModel){
            angular.forEach(shipModel.crew, function(crewModel){
                crewService.tick(shipModel, crewModel);
            });
        }
    };
    
    return shipService;
});