angular.module('vectorService', []).service('vectorService', function(){
    // TODO: Is there a better way of documenting these?  Types don't show up.
    /**
     * The location on the X axis, where - is left, and + is right.
     * @name vector#x
     * @type {number}
     */
     
    /**
     * The location on the Y axis, where - is down, and + is up.
     * @name vector#y
     * @type {number}
     */     
     
    /**
     * Contains functions for performing two-dimensional vectorService math operations.
     * @name vectorService
     * @namespace
     */     
    var vectorService = this;
    
    /**
     * Copies x and y from one vectorService to another.
     * @param {vector} input The vectorService to copy x and y from.
     * @param {vector} out The vectorService to copy x and y to.
     */
    vectorService.clone = function(input, out){
        out.x = input.x;
        out.y = input.y;
    };
    /**
     * Adds two vectorServices together, storing the result in a vectorService.
     * @param {vector} a The first vectorService to add.
     * @param {vector} b The second vectorService to add.
     * @param {vector} out The vectorService to store the result in.  
     * This may be equal to a or b.
     */
    vectorService.add = function(a, b, out){
        out.x = a.x + b.x;
        out.y = a.y + b.y;
    };
    /**
     * Subtracts one vectorService from another, storing the result in a vectorService.
     * @param {vector} a The vectorService to subtract from.
     * @param {vector} b The vectorService to subtract by.
     * @param {vector} out The vectorService to store the result in.  
     * This may be equal to a or b.
     */    
    vectorService.subtract = function(a, b, out){
        out.x = a.x - b.x;
        out.y = a.y - b.y;
    };
    /**
     * Multiplies two vectorServices together, storing the result in a vectorService.
     * @param {vector} a The first vectorService to multiply.
     * @param {vector} b The second vectorService to multiply.
     * @param {vector} out The vectorService to store the result in.  
     * This may be equal to a or b.
     */    
    vectorService.multiply = function(a, b, out){
        out.x = a.x * b.x;
        out.y = a.y * b.y;
    };
    /**
     * Subtracts one vectorService by another, storing the result in a vectorService.
     * @param {vector} a The vectorService to divide.
     * @param {vector} b The vectorService to divide by.
     * @param {vector} out The vectorService to store the result in.  
     * This may be equal to a or b.
     */       
    vectorService.divide = function(a, b, out){
        out.x = a.x / b.x;
        out.y = a.y / b.y;
    };
    /**
     * Calculates the sum of the components of a vectorService.
     * @param {vector} input The vectorService to sum the components of.
     * @returns {number} The sum of the components in input.
     */
    vectorService.componentSum = function(input){
        return input.x + input.y;
    };
    /**
     * Calculates the dot product of two vectorServices.
     * @param {vector} a The first vectorService to calculate the dot product of.
     * @param {vector} b The second vectorService to calculate the dot product of.
     * @returns {number} The dot product of a and b.
     */
    vectorService.dotProduct = function(a, b){
        return a.x * b.x + a.y * b.y;
    };
    /**
     * Calculates the square of the magnitude of a vectorService.
     * Faster than calculating the magnitude.
     * @param {vector} input The vectorService to calculate the square of the magnitude of.
     * @returns {number} The square of the magnitude of input.
     */
    vectorService.magnitudeSquared = function(input){
        return vectorService.dotProduct(input, input);
    },   
    /**
     * Calculates the square of the magnitude of a vectorService.
     * Slower than calculating the square of the magnitude.
     * @param {vector} input The vectorService to calculate the magnitude of.
     * @returns {number} The magnitude of input.
     */
    vectorService.magnitude = function(input){
        return Math.sqrt(vectorService.magnitudeSquared(input));
    }       
});