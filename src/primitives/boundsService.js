angular.module('boundsService', []).service('boundsService', function(){
    
    /**
     * The inclusive lower bound on the X axis.
     * @name bounds#left
     * @type {number}
     */
     
    /**
     * The exclusive upper bound on the X axis.
     * @name bounds#right
     * @type {number}
     */     
     
    /**
     * The inclusive lower bound on the Y axis.
     * @name bounds#bottom
     * @type {number}
     */
     
    /**
     * The exclusive upper bound on the Y axis.
     * @name bounds#top
     * @type {number}
     */      
     
    /**
     * Contains functions for handling two-dimensional axis-aligned-bounding-boxes.
     * @name boundsService
     * @namespace
     */       
    
    var boundsService = {};
    
    /**
     * Copies x and y from one bounds to another.
     * @param {bounds} input The bounds to copy from.
     * @param {bounds} out The bounds to copy to.
     */
    boundsService.clone = function(input, out){
        out.left = input.left;
        out.right = input.right;
        out.top = input.top;
        out.bottom = input.bottom;
    };
    
    /**
     * Determines whether a given vector falls within given bounds.
     * @param {bounds} bounds The bounds to check within.
     * @param {number} x The location of the point to check against the given 
     * bounds on the X axis.
     * @param {number} y The location of the point to check against the given
     * bounds on the Y axis.
     */
    boundsService.contains = function(bounds, x, y){
        if(x < bounds.left) return false;
        if(x >= bounds.right) return false;
        if(y < bounds.bottom) return false;
        if(y >= bounds.top) return false;        
        return true;
    };    
    
    /**
     * A callback which can be given to iterate.
     * @callback iterateCallback
     * @param {number} The current location, on the X axis.
     * @param {number} The current location, on the Y axis.
     * @returns If undefined is returned, the iteration continues.  Any other
     * value aborts iteration and returns that value from the call to iterate
     * immediately.
     */    
    
    /**
     * Executes a callback with every location within given bounds. Breaks early 
     * and returns the value returned if the callback returns non-undefined.
     * @param {bounds} bounds The bounds to iterate through.
     * @param {iterateCallback} callback A callback to execute for each location
     * in the given bounds.
     */
    boundsService.iterate = function(bounds, callback){
        var x, y;
        for(y = bounds.bottom; y < bounds.top; y++) {
            for(x = bounds.left; x < bounds.right; x++) {
                var result = callback(x, y);
                if(result !== undefined) return result;
            }
        }
    };    
    
    return boundsService;
    
})