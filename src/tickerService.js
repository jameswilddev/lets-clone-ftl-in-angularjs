
angular.module('tickerService', []).service('tickerService', function($interval){

    /** 
     * A service allowing for centralized, pausable game state ticks.
     * @name tickerService
     * @namespace tickerService
     */    
    var tickerService = {};
    
    /**
     * The list of functions to execute on each tick.  Add or remove to register
     * and deregister functions.
     */
    tickerService.toTick = [];
    
    var running = undefined;
    
    /**
     * Returns true when the ticker is paused, and false when it is not.
     */
    tickerService.paused = function(){
        return !running;
    };
    
    var tick = function(){
        angular.forEach(tickerService.toTick, function(toTick){
            toTick();
        });
    };
    
    /**
     * Call to toggle whether the ticker is paused, pausing if running and
     * unpausing if paused.
     */    
    tickerService.togglePause = function(){
        if(running) {
            $interval.cancel(running);
        running = undefined;            
        } else {
            running = $interval(tick, 250);
        }
    };
    
    /**
     * Unpauses the ticker if not running, else does nothing.
     */
    tickerService.unpause = function(){
        if(running) return;
        running = $interval(tick, 250);
    };
    
    /**
     * Pauses the ticker if running, else does nothing.
     */
    tickerService.pause = function(){
        if(!running) return;
        $interval.cancel(running);
        running = undefined;
    };
    
    return tickerService;
});