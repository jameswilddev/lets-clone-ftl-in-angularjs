angular.module('app', ['shipService', 'crewService', 'roomService', 'tickerService', 'shipController'])
.controller('test', function($scope, $http, shipService, crewService, roomService, tickerService, $window, $controller){
    $scope.roomService = roomService;
    $scope.status = 'Downloading template...';
    crewService.whenLoaded(function(){
        $http({
            url: 'data/shipTemplates/basic.json',
            method: 'GET'
        }).success(function(data){
            $scope.status = 'Importing...';
            $scope.ship = shipService.import(data);
            //$scope.ship.fires.push({x: 2, y: 3});
            var controller = $controller('shipController', {$scope: $scope});
            $scope.init($scope.ship);
            $scope.status = 'Successful.';
            //tickerService.toTick.push(function(){shipService.tick($scope.ship);});
        }).error(function(data){
            $scope.status = 'Failed: ' + data;
        })
    });
    $scope.addCrew = function(){
        var location = {};
        if(shipService.findEmptyTile($scope.ship, true, true, location)){
            $scope.crew.push(crewService.create(location.x, location.y, location.roomId));
        } else $window.alert('No free spaces to add a new crewmember');
    };
    $scope.tickerService = tickerService;
});