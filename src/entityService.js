angular.module('entityService', ['vectorService']).service('entityService', function(vectorService){
   
    /**
     * A service for performing generic operations on entities such as
     * crewmembers.
     */
    var entityService = {};
   
    /**
     * Finds the entity, if any, at a given location, otherwise returning 
     * undefined.
     */
    entityService.findAt = function(entities, x, y){
        var entityId;
        for(entityId = 0; entityId < entities.length; entityId++){
            var entityModel = entities[entityId];
            if(entityModel.x != x) continue;
            if(entityModel.y != y) continue;
            return entityModel;
        }
        return undefined;
    };
    
    /**
     * Approaches a given location, moving one tile closer to it if not already at that location.
     * @param {EntityModel} entityModel The entity to move.
     * @param {Vector} location The location to approach.
     * @returns {Boolean} Whether a move was made; if the entity is in position, no move is made.
     */
    entityService.approach = function(entityModel, location){
        if(entityModel.x < location.x) {
            entityModel.x++;
            return true;
        }
        if(entityModel.x > location.x) {
            entityModel.x--;
            return true;
        }        
        if(entityModel.y < location.y) {
            entityModel.y++;
            return true;
        }        
        if(entityModel.y > location.y) {
            entityModel.y--;
            return true;
        }          
        return false;
    };
    
    return entityService;
    
});