angular.module('random', []).service('random', function(){
    
    /** 
     * A service providing a mockable source of random data.
     * @name random
     * @namespace random
     */        
    var random = {};
    
    /**
     * Retrieves a random number between 0 (inclusive) and 1 (exclusive).
     * Equivalent to Math.random().
     * @function
     */
    random.float = Math.random;
    
    /**
     * Retrieves a random number between two inclusive integer bounds.
     */
    random.integer = function(min, max){
        return Math.floor((Math.random() * (max - min)) + min);
    };
    
    return random;
})