angular.module('doorService', []).service('doorService', function(){
    
    /** @nameSpace doorService */
    var doorService = {
        /** 
         * @memberOf doorService
         * @param {ShipTemplate} shipTemplate
         * @param {DoorTemplate} doorTemplate
         * @returns {DoorModel}
         */        
        import: function(shipTemplate, doorTemplate){
            var doorModel = {
                x: doorTemplate.x,
                y: doorTemplate.y,
                vertical: doorTemplate.vertical
            };
            
            // Find rooms which this door borders.
            var roomId;
            for(roomId = 0; roomId < shipTemplate.rooms.length; roomId++){
                var roomTemplate = shipTemplate.rooms[roomId];
            }
            
            return doorModel;
        },
        
        /**
         * Finds the room on the other side of a door, if any.
         * @memberOf doorService
         * @param {DoorModel} doorModel
         * @param {Integer} roomId The id of the room coming from.
         * @returns {Integer} The id of the room on the other side of the door, 
         * if any, else returns undefined.  If undefined, the door is considered 
         * to vent into space.
         */        
        getOtherRoomId: function(doorModel, roomId){
            if(doorModel.fromRoomId == roomId) return doorModel.toRoomId;
            if(doorModel.toRoomId == roomId) return doorModel.fromRoomId;
            return undefined;
        },   
        
        /**
         * @memberOf doorService
         * @callback iterateConnectingRoomsCallback
         * @param {Integer} The id of the connecting room.
         */  
         
        /**
         * Iterates through each room which can be accessed from a given room by
         * finding all matching doors, executing a callback for each.
         * @memberOf doorService
         * @param {ShipModel} shipModel
         * @param {Integer} roomId
         * @param {IterateConnectingRoomsCallback} iterateConnectingRoomsCallback A 
         * callback to execute for each room connecting to that specified by roomId.
         */         
        iterateConnectingRooms: function(shipModel, roomId, callback){
            angular.forEach(shipModel.doors, function(doorModel){
                var otherSide = doorService.getOtherRoomId(doorModel, roomId);
                if(otherSide === undefined) return;
                callback(otherSide);
            });
        },
        
        /**
         * @nameSpace doorService.roomMatching
         */
        roomMatching: {
            /**
             * A constant value denoting the left border of a room.  
             * @memberOf doorService.roomMatching
             * @type MatchResult
             */
            left: {},
            
            /**
             * A constant value denoting the right border of a room.
             * @memberOf doorService.roomMatching
             * @type MatchResult
             */    
            right: {},
            
            /**
             * A constant value denoting the top border of a room.
             * @memberOf doorService.roomMatching
             * @type MatchResult
             */    
            top: {},
            
            /**
             * A constant value denoting the bottom border of a room.
             * @memberOf doorService.roomMatching
             * @type MatchResult
             */    
            bottom: {},
            
            /**
             * Compares a room to a door and determines how they intersect.
             * @memberOf doorService.roomMatching
             * @param {RoomTemplate} roomTemplate The room to check for intersection.
             * @param {DoorTemplate} doorTemplate The door to check for intersection.
             * @returns {MatchResult} If there is no intersection between the door and room, undefined.
             * Else, a MatchResult identifying the boundary on which the door and room intersect.
             */
            match: function(roomTemplate, doorTemplate){
                if(doorTemplate.vertical){
                    if(doorTemplate.y < roomTemplate.bottom) return;
                    if(doorTemplate.y >= roomTemplate.top) return;
                    if(doorTemplate.x == roomTemplate.left) return doorService.roomMatching.left;
                    if(doorTemplate.x == roomTemplate.right) return doorService.roomMatching.right
                } else {
                    if(doorTemplate.x < roomTemplate.left) return;
                    if(doorTemplate.x >= roomTemplate.right) return;                
                    if(doorTemplate.y == roomTemplate.top) return doorService.roomMatching.top;
                    if(doorTemplate.y == roomTemplate.bottom) return doorService.roomMatching.bottom;
                }
            }   
        }
    };

    
    return doorService;
    
});