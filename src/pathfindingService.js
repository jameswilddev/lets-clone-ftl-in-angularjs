angular.module('pathfindingService', ['vectorService', 'doorService', 'roomService']).service('pathfindingService', function(vectorService, doorService, roomService){
    
    /**
     * Provides services for finding routes for an entity to take through a ship.
     * @name pathfindingService
     * @namespace
     */       
    var pathfindingService = {};
    
    /**
     * Finds a route through a given ship for a given entity.
     * The location of the door to walk to in the current room will be stored in
     * entityModel.targetLocation.x/y.
     * You may call this at any time during the move to re-evaluate the route.
     * Pathfinding will automatically prioritize safe rooms over dangerous rooms,
     * even if the route is considerably longer.
     * It also considers whether doors are usable at all.
     * @param {shipModel} shipModel The ship to find a route through.
     * @param {entityModel} entityModel An entity to provide the route to. 
     */    
    pathfindingService.route = function(shipModel, entityModel){
        // TODO: for the moment, we just copy the target location on a room
        // level directly from the target location on a ship level.  This causes
        // crewmen to walk through walls to reach their destinations.
        if(entityModel.roomId == entityModel.displayLocation.roomId) {
            vectorService.clone(entityModel, entityModel.targetLocation);
            return;
        }
        
        var bestMatches = [];
        var recurseRooms = function(roomId, danger, doors, parentMatch){
            var roomModel = shipModel.rooms[roomId];
            var matchId, match;
            for(matchId = 0; matchId < bestMatches.length; matchId++){
                match = bestMatches[matchId];
                if(match.roomId == roomId) {
                    if(match.danger < danger) return;
                    if(match.danger == danger && match.doors <= doors) return;
                    break;
                }
            }
            if(matchId == bestMatches.length){
                match = {
                    roomId: roomId
                };
                bestMatches.push(match);
            }
            match.danger = danger;
            match.doors = doors;
            match.parentMatch = parentMatch;
            angular.forEach(roomModel.doors, function(doorLookupModel){
                if(doorLookupModel.roomId === undefined) return;
                recurseRooms(doorLookupModel.roomId, roomService.dangerous(shipModel, shipModel.rooms[doorLookupModel.roomId]) ? danger + 1 : danger, doors + 1, match);
            });
        };
        recurseRooms(entityModel.displayLocation.roomId, 0, 0, undefined);
        var match, matchId;
        for(matchId = 0; matchId < bestMatches.length; matchId++){
            match = bestMatches[matchId];
            if(match.roomId == entityModel.roomId) {
                break;
            }
        }
        while(match.parentMatch != bestMatches[0]){
            match = match.parentMatch;
        }

        var roomModel = shipModel.rooms[entityModel.displayLocation.roomId];
        var doorLookupId;
        for(doorLookupId = 0; doorLookupId < roomModel.doors.length; doorLookupId++){
            var doorLookupModel = roomModel.doors[doorLookupId];
            if(match.roomId == doorLookupModel.roomId) {
                vectorService.clone(doorLookupModel, entityModel.targetLocation);
                entityModel.targetLocation.doorLookupId = doorLookupId;
                return;
            }
        }
    };
    
    return pathfindingService;
    
});