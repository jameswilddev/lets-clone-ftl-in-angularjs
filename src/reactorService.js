angular.module('reactorService', []).service('reactorService', function(){

    /** 
     * A service implementing the reactor, handling power allocations.
     * @name reactorService
     * @namespace reactorService
     */    
    var reactorService = {};

    /**
     * Initializes a ship model with the fields needed to use the reactor.
     */
    reactorService.initialize = function(shipModel){
        shipModel.reactor = {
            allocated: 0,
            total: 5
        };
    };
    
    /**
     * Attempts to allocate a specifed amount of power from the reactor's
     * unallocated reserves.  Returns true if the amount could be allocated,
     * or false if it could not.
     */
    reactorService.requestPower = function(shipModel, amount){
        if(shipModel.reactor.allocated + amount > shipModel.reactor.total) return false;
        shipModel.reactor.allocated += amount;
        return true;
    };
    
    /**
     * Deallocates a previously allocated amount of power, allowing it to be
     * allocated again.
     */
    reactorService.releasePower = function(shipModel, amount){
        shipModel.reactor.allocated -= amount;
        return true;
    };

    return reactorService;
});