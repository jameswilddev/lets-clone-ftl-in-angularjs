angular.module('fireService', ['random']).service('fireService', function(random){
    
    /**
     * A service for handling fires in a ship.
     * @name fireService
     * @namespace fireService
     */
    var fireService = {};
    
    /**
     * Creates a new fire at a given ship tile location.
     */
    fireService.create = function(x, y){
        return {
            x: x,
            y: y,
        };
    };
    
    /**
     * Updates a given fire.  To be called once per tick.
     */
    fireService.tick = function(shipModel, fireModel){
        
    };
    
    return fireService;
    
});